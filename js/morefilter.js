var morefilter = {
    positiveCriterias: [],
    negativeCriterias: [],
    operator: "and",
    
    execute: function(criteria, type){
	var btn = $("#"+criteria);
	var action = (type) ? type : btn.attr('action');
	var currentPosCriteria = this.positiveCriterias;
	var currentNegCriteria = this.negativeCriterias;
	var indexInPos = currentPosCriteria.indexOf(criteria);
	var indexInNeg = currentNegCriteria.indexOf(criteria);
	
	switch (action) { 
	case 'off':
	    if (indexInPos != -1 ){
		currentPosCriteria.splice(indexInPos, 1);
	    }
	    
	    if (indexInNeg != -1 ){
		currentNegCriteria.splice(indexInNeg, 1);
	    }
	    button.disable(btn);
	    break;
	case 'no':
	    if (indexInPos != -1 ){
		currentPosCriteria.splice(indexInPos, 1);
	    }
	    
	    if (indexInNeg == -1 ){
		currentNegCriteria.push(criteria);
	    }
	    button.disable(btn);
	    break;
	default: //case 'on':
	    if (indexInPos == -1 ){
		currentPosCriteria.push(criteria);
	    }
	    
	    if (indexInNeg != -1 ){
		currentNegCriteria.splice(indexInNeg, 1);
	    }
	    button.enable(btn);
	    break;
	}
	var nextAction = filter.nextAction(action);
	btn.attr('action', nextAction);
	this.apply();
	navigate.init();

	/*alert("Btn action: "+action+
	      "\nCurrent criteria: "+criteria+
	      "\nPositive filters: "+currentPosCriteria+
	      "\nNegative filters: "+currentNegCriteria+
	      "\nNext action: "+nextAction+
	      "\nOperator: "+operator);*/

	//alert("More filters END !");
    },
    reset: function(){
	$("#operator label input[value='and']").trigger("click");
	button.disableAll();
	$(".btn-filter").attr("action", "on");
	
	this.positiveCriterias = [];
	this.negativeCriterias = [];
	this.operator="and";
	this.apply();
	navigate.init();
	//navigate.collapseAll(); //I don't know which behaviour is the best: Not doing that allows to open some comment, remove filters and leave "selected" comments opened, while seing others closed. It could be interesting...? And if one wants to close all comments, s/he can simply use the switch
    },
    apply: function(){
	var comments = $('.comment');
	var currentPosCriteria = this.positiveCriterias;
	var currentNegCriteria = this.negativeCriterias;
	var currentOperator = this.operator;

	comments.hide();
	switch (this.operator){
	case  "or":
	    var listCriteria = "";
	    if (currentPosCriteria.length > 0){
		for (var i = 0; i < currentPosCriteria.length; i++){
		    var criteria = currentPosCriteria[i];
		    listCriteria += "\n.comment."+criteria;
		    $(".comment."+ criteria).show();
		}
	    }
	    if (currentNegCriteria.length > 0){
		for (var i = 0; i < currentNegCriteria.length; i++){
		    var criteria = currentNegCriteria[i];
		    listCriteria += "\n.comment:not(."+criteria+")";
		    $(".comment:not(."+ criteria+")").show();
		}
	    }
	    if (currentPosCriteria.length+currentNegCriteria.length <= 0){
		$('.comment').show();
	    }
	    //alert("Shown are:\n"+listCriteria);
	    break;
	case "and":
	    var concatCriterias = "";
	    if (currentPosCriteria.length > 0){
		for (var i = 0; i < currentPosCriteria.length; i++){
		    concatCriterias += "."+currentPosCriteria[i];
		}
	    }
	    if (currentNegCriteria.length > 0){
		for (var i = 0; i < currentNegCriteria.length; i++){
		    concatCriterias += ":not(."+currentNegCriteria[i]+")";
		}
	    }
	    //alert("Shown are:\n.comment"+concatCriterias);
	    $(".comment"+concatCriterias).show();
	    break;
	}
    },
    load:  function(){
	$(".comment").show();
	var operator = url.getParameter('op');
	if (op){
	    $("#operator label input[value='" + operator + "']").trigger("click");
	    this.operator=operator;
	}
	
	var filterParam = url.getParameter("filter_is");
	if (filterParam) {
	    var filters = filterParam.split(",");
	    for (var i = 0; i < filters.length; i++) {
		var filterId = filters[i];
		if (filterId != "") {
		    this.execute(filterId, "on");
		}
	    }
	}
	var filterParam = url.getParameter("filter_isnot");
	if (filterParam) {
	    var filters = filterParam.split(",");
	    for (var i = 0; i < filters.length; i++) {
		var filterId = filters[i];
		if (filterId != "") {
		    this.execute(filterId, "no");
		}
	    }
	}
    },
    save: function(){
	var currentPosCriteria = this.positiveCriterias;
	var currentNegCriteria = this.negativeCriterias;
	var currentOperator = this.operator;

	var operator = "&op="+currentOperator;

	var posFilter = "&filter_is=";
	for (var i = 0; i < currentPosCriteria.length; i++) {
	    posFilter += (i > 0) ? "," + currentPosCriteria[i] : currentPosCriteria[i];
	}

	var negFilter = "&filter_isnot=";
	for (var i = 0; i < currentNegCriteria.length; i++) {
	    negFilter += (i > 0) ? "," + currentNegCriteria[i] : currentNegCriteria[i];
	}

	var cleanUrl = url.removeParameter(null, "filter_is");
	cleanUrl= url.removeParameter(cleanUrl, "filter_isnot");
	cleanUrl= url.removeParameter(cleanUrl, "op");

	return newUrl = cleanUrl + posFilter + negFilter + operator;
    },
    nextAction: function(current){
	if (current === undefined) {
	    current="on";
	}
	// on => no => off
	var next = "NA";
	switch (current) {
	case 'on':
	    next="no";
	    break;
	case 'no':
	    next="off";
	    break;
	case 'off':
	    next='on';
	    break;
	default:
	    next='no';
	}
	return next;
    }
}
