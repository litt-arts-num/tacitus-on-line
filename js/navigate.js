function highlight(id){
    document.getElementById(id).classList.add("highlighted");
}
function unhighlight(id){
    document.getElementById(id).classList.remove("highlighted");
}
function highlight_class(html_class){
    selection = document.getElementsByClassName(html_class);
    for (var i = 0; i < selection.length; i++){
        selection[i].classList.add("highlighted");
    }
}
function unhighlight_class(html_class){
    selection = document.getElementsByClassName(html_class);
    for (var i = 0; i < selection.length; i++){
        selection[i].classList.remove("highlighted");
    }
}
var navigate = {
    selection: null,
    
    init: function(scroll) {
	$("#nav-box > div").show();
	var selection = $('.comment').filter(function() { return $(this).css("display") != "none"; });
	if (selection.length > 0) {
            this.selection = selection;
            this.length = selection.length;
            this.position = 0;
            this.updateContent();
            for (var i = 0; i < this.length; i++) {
		this.selection[i].classList.remove("border-dark");
            }
            this.selection[this.position].classList.add("border-dark");
            if (scroll) this.scrollTo();
	}
	return this.selection;
    },
    
    next: function() {
	if (this.position >= this.length){
	    this.position = 0;
	    this.selection[this.position].classList.add("border-dark");
	    //this.selection[this.position].getElementsByTagName("div")[2].classList.add("show");
	    this.addShowOnCommentContent(this.selection[this.position]);
	    this.selection[this.length-1].classList.remove("border-dark");
	    document.getElementById("nav-next").classList.remove("disabled");
	    if ($('#nav-expan').attr("status") == "off") {
		//this.selection[this.length-1].getElementsByTagName("div")[2].classList.remove("show");
		this.removeShowOnCommentContent(this.selection[this.length-1]);
	    }

	} else {
	    this.position += 1;
	    this.selection[this.position].classList.add("border-dark");
	    //this.selection[this.position].getElementsByTagName("div")[2].classList.add("show");
	    this.addShowOnCommentContent(this.selection[this.position]);
	    this.selection[this.position-1].classList.remove("border-dark");
	    if ($('#nav-expan').attr("status") == "off") {
		//this.selection[this.position-1].getElementsByTagName("div")[2].classList.remove("show");
		this.removeShowOnCommentContent(this.selection[this.position-1]);
	    }

	}
	
	if (this.position+1 >= this.length){
	    document.getElementById("nav-next").classList.add("disabled");
	}
	if (this.position+1 > 1){
	    document.getElementById("nav-previous").classList.remove("disabled");
	}
	this.scrollTo();
	return;
    },
    
    previous: function() {
	if (this.position <= 0){
	    this.position = this.length-1;
	    this.selection[this.position].classList.add("border-dark");
	    //this.selection[this.position].getElementsByTagName("div")[2].classList.add("show");
	    this.addShowOnCommentContent(this.selection[this.position]);
	    this.selection[0].classList.remove("border-dark");
	    if ($('#nav-expan').attr("status") == "off") {
		//this.selection[0].getElementsByTagName("div")[2].classList.remove("show");
		this.removeShowOnCommentContent(this.selection[0]);
	    }

	} else {
	    this.position -= 1;
	    this.selection[this.position].classList.add("border-dark");
	    //this.selection[this.position].getElementsByTagName("div")[2].classList.add("show");
        this.addShowOnCommentContent(this.selection[this.position]);
	    this.selection[this.position+1].classList.remove("border-dark");
	    if ($('#nav-expan').attr("status") == "off") {
		//this.selection[this.position+1].getElementsByTagName("div")[2].classList.remove("show");
		  this.removeShowOnCommentContent(this.selection[this.position+1]);
	    }
	}

	if (this.position < this.length){ document.getElementById("nav-next").classList.remove("disabled"); }
	if (this.position <= 0){ document.getElementById("nav-previous").classList.add("disabled"); }
	this.scrollTo();
	return;
    },
    
    scrollTo: function() {
	this.selection[this.position].getElementsByTagName("div")[0].scrollIntoView();
	this.updateContent();
	// add an offset
	var scrolledY = window.scrollY;
	window.scroll(0, scrolledY - 90);
	return;			   
    },
    
    updateContent: function() {
	var index = this.position;
	$("#nav-length").html(this.selection.length);
	$("#nav-title").html(this.selection[index].getElementsByTagName("span")[0].textContent);
	$("#nav-position").html(index + 1);
	return;
    },
    
    expanAll: function() {
	for (var i = 0; i < this.length; i++) {
            this.addShowOnCommentContent(this.selection[i]);
	}
	document.getElementById("nav-expan").classList.add("d-none");
	document.getElementById("nav-expan").setAttribute("status", "on");
	document.getElementById("nav-collapse").classList.remove("d-none");
	document.getElementById("nav-collapse").setAttribute("status", "off");
    },
    
    collapseAll: function() {
	for (var i = 0; i < this.length; i++) {
	   this.removeShowOnCommentContent(this.selection[i]);
            
	}
	document.getElementById("nav-expan").classList.remove("d-none");
	document.getElementById("nav-expan").setAttribute("status", "off");
	document.getElementById("nav-collapse").classList.add("d-none");
	document.getElementById("nav-collapse").setAttribute("status", "on");
    },
    
    removeShowOnCommentContent: function(selection){
        var sel = selection.getElementsByTagName("div");
        for (var s = 0 ; s < sel.length; s++){
            if (sel[s].classList.contains("comment_content")){
                sel[s].classList.remove("show");
            }
        }
    },
    
    addShowOnCommentContent: function(selection){
        var sel = selection.getElementsByTagName("div");
        for (var s = 0 ; s < sel.length; s++){
            if (sel[s].classList.contains("comment_content")){
                sel[s].classList.add("show");
            }
        }
    }

};

// PREVIOUS/NEXT CONTROL USING LEFT/RIGHT ARROW
document.addEventListener('keydown',function (e) {
    switch (e.which) {
    case 37:
	navigate.previous();
	break;
	
    case 39:
	navigate.next();
	break;
	
    default:
	return;
    }
    
    e.preventDefault();
});
