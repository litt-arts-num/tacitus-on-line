var filter = {
    positiveCriterias: [],
    negativeCriterias: [],
    operator: "and",
    viewAllParagraph: "on",
    selectedParagraphs : "",
    
    execute: function(criteria, type){
	var btn = $("#"+criteria);
	var action = (type) ? type : btn.attr('action');
	var currentPosCriteria = this.positiveCriterias;
	var currentNegCriteria = this.negativeCriterias;
	var indexInPos = currentPosCriteria.indexOf(criteria);
	var indexInNeg = currentNegCriteria.indexOf(criteria);
	var viewAllParagraph = this.viewAllParagraph;
	
	switch (action) { 
	case 'off':
	    if (indexInPos != -1 ){
		currentPosCriteria.splice(indexInPos, 1);
	    }
	    
	    if (indexInNeg != -1 ){
		currentNegCriteria.splice(indexInNeg, 1);
	    }
	    button.disable(btn);
	    break;
	case 'no':
	    if (indexInPos != -1 ){
		currentPosCriteria.splice(indexInPos, 1);
	    }
	    
	    if (indexInNeg == -1 ){
		currentNegCriteria.push(criteria);
	    }
	    button.disable(btn);
	    break;
	default: //case 'on':
	    if (indexInPos == -1 ){
		currentPosCriteria.push(criteria);
	    }
	    
	    if (indexInNeg != -1 ){
		currentNegCriteria.splice(indexInNeg, 1);
	    }
	    button.enable(btn);
	    break;
	}
	var nextAction = filter.nextAction(action);
	btn.attr('action', nextAction);
	this.apply();

    },
    reset: function(){
	$("#operator label input[value='and']").trigger("click");
	button.disableAll();
	$(".btn-filter").attr("action", "on");
	
	this.positiveCriterias = [];
	this.negativeCriterias = [];
	this.operator="and";
	this.apply();
	//navigate.collapseAll(); //I don't know which behaviour is the best: Not doing that allows to open some comment, remove filters and leave "selected" comments opened, while seing others closed. It could be interesting...? And if one wants to close all comments, s/he can simply use the switch
    },
    apply: function(){
	var comments = $('.comment');
	var currentPosCriteria = this.positiveCriterias;
	var currentNegCriteria = this.negativeCriterias;
	var currentOperator = this.operator;
	
	comments.hide();
	switch (this.operator){
	case  "or":
	    var listCriteria = "";
	    if (currentPosCriteria.length > 0){
		for (var i = 0; i < currentPosCriteria.length; i++){
		    var criteria = currentPosCriteria[i];
		    listCriteria += "\n.comment."+criteria;
		    $(".comment."+ criteria).show();
		}
	    }
	    if (currentNegCriteria.length > 0){
		for (var i = 0; i < currentNegCriteria.length; i++){
		    var criteria = currentNegCriteria[i];
		    listCriteria += "\n.comment:not(."+criteria+")";
		    $(".comment:not(."+ criteria+")").show();
		}
	    }
	    if (currentPosCriteria.length+currentNegCriteria.length <= 0){
		$('.comment').show();
	    }
	    //alert("Shown are:\n"+listCriteria);
	    break;
	case "and":
	    var concatCriterias = "";
	    if (currentPosCriteria.length > 0){
		for (var i = 0; i < currentPosCriteria.length; i++){
		    concatCriterias += "."+currentPosCriteria[i];
		}
	    }
	    if (currentNegCriteria.length > 0){
		for (var i = 0; i < currentNegCriteria.length; i++){
		    concatCriterias += ":not(."+currentNegCriteria[i]+")";
		}
	    }
	    //alert("Shown are:\n.comment"+concatCriterias);
	    $(".comment"+concatCriterias).show();
	    break;
	}
	if (this.viewAllParagraph == "off") {
	    this.showTargetOnly();
	} else {
		$('.paragraph-id').parent().parent().show();
	    $('.subparagraph').parent().show();
	}
	navigate.init();
	
    },
    load:  function(){
	$(".comment").show();
	var operator = url.getParameter('op');
	if (operator){
	    $("#operator label input[value='" + operator + "']").trigger("click");
	    this.operator=operator;
	} else {
	    this.operator="and";
	}
	
	var filterParam = url.getParameter("filter_is");
	if (filterParam) {
	    var filters = filterParam.split(",");
	    for (var i = 0; i < filters.length; i++) {
		var filterId = filters[i];
		if (filterId != "") {
		    this.execute(filterId, "on");
		}
	    }
	}
	var filterParam = url.getParameter("filter_isnot");
	if (filterParam) {
	    var filters = filterParam.split(",");
	    for (var i = 0; i < filters.length; i++) {
		var filterId = filters[i];
		if (filterId != "") {
		    this.execute(filterId, "no");
		}
	    }
	}

	var viewall = url.getParameter("viewall");
	if (viewall == "off"){
	    $("#viewCommentedParagraph").trigger("click");
	    this.viewAllParagraph=viewall;
	} else {
	    this.viewAllParagraph="on";
	}
	
	var plist = url.getParameter("plist");
	if (plist){
	   $("#filterParagraphs").val(plist);
	   $("#doFilterParagraphs").trigger("click");
	   this.plist=plist;
	}
    },
    save: function(){
	var currentPosCriteria = this.positiveCriterias;
	var currentNegCriteria = this.negativeCriterias;
	var currentOperator = this.operator;
	var currentViewAll = this.viewAllParagraph;
	var currentSelectedParagraphs = this.selectedParagraphs;
	
	var operator = "&op="+currentOperator;
	var viewall = "&viewall="+currentViewAll;
	var plist = "&plist="+currentSelectedParagraphs;
	var posFilter = "&filter_is=";
	for (var i = 0; i < currentPosCriteria.length; i++) {
	    posFilter += (i > 0) ? "," + currentPosCriteria[i] : currentPosCriteria[i];
	}
	var negFilter = "&filter_isnot=";
	for (var i = 0; i < currentNegCriteria.length; i++) {
	    negFilter += (i > 0) ? "," + currentNegCriteria[i] : currentNegCriteria[i];
	}

	var cleanUrl = url.removeParameter(null, "filter_is");
	cleanUrl= url.removeParameter(cleanUrl, "filter_isnot");
	cleanUrl= url.removeParameter(cleanUrl, "op");
	cleanUrl= url.removeParameter(cleanUrl, "viewall");
	cleanUrl= url.removeParameter(cleanUrl, "plist");

	newUrl = cleanUrl + posFilter + negFilter + operator + viewall + plist;
	return newUrl;
    },
    nextAction: function(current){
	if (current === undefined) {
	    current="on";
	}
	// on => no => off
	var next = "NA";
	switch (current) {
	case 'on':
	    next="no";
	    break;
	case 'no':
	    next="off";
	    break;
	case 'off':
	    next='on';
	    break;
	default:
	    next='no';
	}
	return next;
    },
    showTargetOnly: function(){
		var selectionCommentaries = $('.comment').filter(function() { return $(this).css("display") != "none"; });
		var selectionParagraphs = getList(document.getElementById('filterParagraphs').value);
		//console.log("§ " + selectionParagraphs + "(" + selectionParagraphs.length + ")");
		$('.subparagraph').parent().hide();
		$('.paragraph-id').parent().parent().hide();
		
		for (var i = 0; i < selectionCommentaries.length; i++){
			var paragraphId = selectionCommentaries[i].getAttribute('data-target').split(" ")[1].substr(9);
			//console.log("pId: " + paragraphId);
			if (selectionParagraphs.length === 0 || selectionParagraphs.includes(parseInt(paragraphId))){
				$('.paragraph-id[data-paragraph-id="TACITUS_n'+paragraphId+'"]').parent().parent().show();
				var subParagraphId = selectionCommentaries[i].getAttribute('data-target').split(" ")[0].substr(11);
				//console.log("spId: " + subParagraphId);
				$('.subparagraph[data-subparagraph-id="'+subParagraphId+'"]').parent().show();
			}
		}
	}
}

function range(start, end){
    var out = new Array();
    for (var i = start; i <= end; i++) {
        out.push(i);
    }
    return out;
};

function getList(selection){
	if (selection === ""){
	    return new Array();
	}
    var lstElem = new Array();
    var lst = selection.split(',');
    
    for (i = 0; i < lst.length; i++) {
        /*  if range, enumerate (eg. 2-4 => [2,3,4]) */
        if (lst[i].includes('-')){
            elem = lst[i].split('-');
            if (parseInt(elem[0]) < parseInt(elem[1])){
                newElem = range(parseInt(elem[0]),parseInt(elem[1]));
                lstElem = lstElem.concat(newElem);
            } else {
                newElem = range(parseInt(elem[1]),parseInt(elem[0]));
                lstElem = lstElem.concat(newElem);
            }
        } else {
            lstElem.push(parseInt(lst[i]));
        }
    }
    /* remove duplicates */
    var unique = [];
    $.each(lstElem, function(i, el){
        if($.inArray(el, unique) === -1) unique.push(el);
    });
    return unique;
}

