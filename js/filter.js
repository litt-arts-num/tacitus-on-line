var simplefilter = {
  activeQuotes: [],
  activeCriterias: [],
  activeAuthors: [],
  operator: "and",

  execute: function(criteria) {
    var btn = $("#" + criteria);
    var criteriaType = btn.data("criteria-type");
    switch (criteriaType) {
      case 'type':
        arrayToUse = this.activeCriterias
        break;

      case 'quote':
        arrayToUse = this.activeQuotes
        break;

      case 'author':
        arrayToUse = this.activeAuthors
        break;
    }

    var index = arrayToUse.indexOf(criteria);

    if (index != -1) {
      arrayToUse.splice(index, 1);
      button.disable(btn);
    } else {
      arrayToUse.push(criteria);
      button.enable(btn);
    }

      this.apply();
      navigate.init();
  },

  reset: function() {
    $("#operator label input[value='and']").trigger("click");
    this.activeCriterias = [];
    this.activeAuthors = [];
    this.activeQuotes = [];
    this.apply();
    button.disableAll();
  },

  apply: function() {
    var comments = $(".comment");

    comments.hide();
    comments.attr("data-show-quote", "false");
    comments.attr("data-show", "false");

    var quotes = filter.activeQuotes;
    if (quotes.length > 0) {
      for (var i = 0; i < quotes.length; i++) {
        var quote = quotes[i];
        $(".comment[data-quote-type*='" + quote + "']").attr("data-show-quote", "true");
      }
    } else {
      comments.attr("data-show-quote", "true");
    }

    var authors = filter.activeAuthors;
    if (authors.length > 0) {
      for (var i = 0; i < authors.length; i++) {
        var author = authors[i];
        $(".comment[data-show-quote='true'][data-author='" + author + "']").attr("data-show", "true");
      }
    } else {
      $(".comment[data-show-quote='true']").attr("data-show", "true");
    }

    if (this.operator == "or") {
      if (this.activeCriterias.length > 0) {
        for (var i = 0; i < this.activeCriterias.length; i++) {
          var criteria = this.activeCriterias[i];
          $(".comment[data-show='true']." + criteria).show();
        }
      } else {
        $(".comment[data-show='true']").show();
      }
    } else {
      var criterias = "";
      for (var i = 0; i < this.activeCriterias.length; i++) {
        var criteria = this.activeCriterias[i];
        criterias += "." + criteria;
      }
      $(".comment[data-show='true']" + criterias).show();
    }
  },

  load: function() {
    var operator = url.getParameter('op');
    $("#operator label input[value='" + operator + "']").trigger("click");

    var filterParam = url.getParameter('filters');
    if (filterParam) {
      var filters = filterParam.split(",");
      for (var i = 0; i < filters.length; i++) {
        var filterId = filters[i];
        if (filterId != "") {
          this.execute(filterId);
        }
      }
    }
  },

  save: function() {
    var criterias = this.activeCriterias.concat(this.activeAuthors).concat(this.activeQuotes);
    var operator = "&op=" + this.operator;
    var filters = "&filters=";
    for (var i = 0; i < criterias.length; i++) {
      filters += (i > 0) ?
        "," + criterias[i] :
        criterias[i];
    }
    var cleanUrl = url.removeParameter(null, "filters");
    cleanUrl = url.removeParameter(cleanUrl, "op");

    return newUrl = cleanUrl + "" + filters + operator;
  },
};
