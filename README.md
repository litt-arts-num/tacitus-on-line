# ⭐ **Ce projet a été déplacé sur [l'instance git de l'UGA](https://gricad-gitlab.univ-grenoble-alpes.fr/elan/tacitus-on-line)** ⭐

# Installation
Dependency // Dépendances
  * composer : https://getcomposer.org/
  * npm : https://www.w3schools.com/whatis/whatis_npm.asp

Installation
```bash
git clone git@gitlab.com:litt-arts-num/tacitus-on-line.git
cd tacitus-on-line
composer install
npm install
npm run build
```

# Usage
Browse to: // Ouvrir dans son navigateur :

`$PATH/tacitus-on-line/` (e.g. `http://localhost/tacitus-on-line/`)

# Update platform // Mise à jour du site
```bash
git pull origin master
composer install
npm install
npm run build
```

# Update data // Mise à jour des données
## Data organization // Organisation des données
<pre><font color="#0000AA"><b>data/</b></font>
├── <font color="#0000AA"><b>tei</b></font><font color="#AF0000"> # All the TEI files</font>
│   ├── <font color="#0000AA"><b>Belles_Lettres</b></font><font color="#AF0000"> # Data from Belles Lettres</font>
│   │   └── Annales_de_Tacite.xml <font color="#AF0000"> # Tacitus text</font>
│   ├── <font color="#0000AA"><b>Encodage</b></font><font color="#AF0000"> # Encoded commentaries files</font>
│   │   ├── <font color="#0000AA"><b>Entetes</b></font><font color="#AF0000"> # All teiHeader files</font>
│   │   ├── <font color="#0000AA"><b>Livre_I</b></font><font color="#AF0000"> # Commentaries to _Annales_ book 1</font>
│   │   ├── <font color="#0000AA"><b>Livre_II</b></font><font color="#AF0000"> # Commentaries to _Annales_ book 2</font>
│   │   └── <font color="#0000AA"><b>Livre_III</b></font><font color="#AF0000"> # Commentaries to _Annales_ book 3</font>
│   └── Tacitus_on_line.xml<font color="#AF0000"> # General file including all the other</font>
└── <font color="#0000AA"><b>xslt</b></font><font color="#AF0000"> # Transformation files</font>
    ├── tacitusAnchors.xsl
    ├── tacitusCommentariesTypologie.xsl
    ├── tacitusCommentateursFrise.xsl
    ├── tacitusCommentateursListe.xsl
    ├── tacitusCo-occurences.xsl
    ├── tacitusFilters.xsl
    ├── tacitusFrise.xsl
    ├── tacitusGenerateAll.xsl
    ├── tacitusHeader.xsl
    ├── tacitusInventaire.xsl
    ├── tacitus_Manual-AuthorsList.xsl
    ├── tacitusOnline.xsl
    ├── tacitus_quoteAuthors.xsl
    └── tacitusTable.xsl
</pre>

## Updating editorial contents
* For the home page: update content in teiHeader files (`data`) and regenerate outputs (see bellow)


## Regenerating webpages
```bash
# It always a good idea to pull all new content from git
git pull origin master

# Run XSL Transformations (all or part of the following)
  # to be completed
  * XML file: data/tei/Tacite_BellesLettres_ET_Commentaires.xml
  * XSL : data/xslt/tacitusOnline.xsl
  * Output: data/xslt/output.log
# Check that everything is ok on your local browser: `$PATH/tacitus-on-line/` (e.g. `http://localhost/tacitus-on-line/`)

# Commit and push to git
git commit -m "Regenerate output" .
git push
# Deploy on the serveur : connect to the server and see "Update platform" section above
```

