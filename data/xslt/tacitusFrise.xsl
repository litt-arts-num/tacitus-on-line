<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tei" version="2.0"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0">
    <xsl:output method="html" omit-xml-declaration="yes" indent="yes"/>

    <xsl:param name="typeMap">
        <xsl:copy-of select="//tei:list[@xml:id = 'typology_commentary']"/>
    </xsl:param>

    <xsl:template match="TEI">
        <div id="legend">
            <h4>Mode d'emploi et légende</h4>
            <ul style="list-style-type: none;" class="pull-right">
                <li>
                    <b>Légende</b>
                </li>
                <xsl:for-each select="$typeMap/tei:list/tei:item">
                    <li>
                        <span>
                            <xsl:attribute name="data-label">
                                <xsl:value-of select="@xml:id"/>
                            </xsl:attribute>
                            <xsl:attribute name="title">
                                <xsl:text>Commentaire de type "</xsl:text>
                                <xsl:value-of
                                    select="concat(upper-case(substring(tei:name, 1, 1)), substring(tei:name, 2))"/>
                                <xsl:text>", encodé en TEI dans l'attribut @ana avec la valeur "</xsl:text>
                                <xsl:value-of select="@xml:id"/>
                                <xsl:text>"</xsl:text>
                            </xsl:attribute> ⬤ </span>
                        <!--<xsl:text> : </xsl:text>-->
                        <xsl:value-of
                            select="concat(upper-case(substring(tei:name, 1, 1)), substring(tei:name, 2))"
                        />
                    </li>
                </xsl:for-each>
            </ul>
            <p>
                <span class="lead">Les frises</span>
                 représentent pour chaque sous-paragraphe du texte de Tacite, les commentaires
                associés. Pour chaque commentateur sont représentés les commentaires par une ou
                plusieurs gommettes colorées en fonction du type de commentaire dont il s'agit.<br/>
                Il est possible de faire défiler chaque frise en utilisant l'ascenseur horizontal
                sous celle-ci. </p>
            <p>
                <span class="lead">Les couleurs</span>
                 correspondent au type des commentaires. La légende indique la corespondance
                utilisée. Cette information est aussi disponible en survolant les gommettes. </p>
            <p>
                <span class="lead">Une navigation</span>
                 est possible vers les commentaires en cliquant sur les gommettes ou bien vers les
                paragraphes du texte de Tacite en cliquant sur les identfiants de paragraphe.</p>
        </div>
        <xsl:apply-templates select="text/front"/>
    </xsl:template>

    <xsl:template match="teiHeader"/>

    <xsl:template match="text/front">
        <xsl:for-each select="//div[@type = 'libre']">
            <xsl:variable name="liber">
                <xsl:value-of select="@xml:id"/>
            </xsl:variable>
            <h4>Livre <xsl:value-of select="substring($liber, 2)"/></h4>
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Tacitus</th>
                            <xsl:for-each
                                select="child::div[@type = 'paragraph']/span[@type = 'sub-paragraph']">
                                <th style="writing-mode: tb;" class="p-1">
                                    <a>
                                        <xsl:attribute name="href">
                                            <xsl:text>?page=livre</xsl:text>
                                            <xsl:value-of select="substring($liber, 2)"/>
                                            <xsl:text>#TACITUS_</xsl:text>
                                            <xsl:value-of select="@xml:id"/>
                                        </xsl:attribute>
                                        <xsl:value-of select="substring-after(@xml:id, 'n')"/>
                                    </a>
                                </th>
                            </xsl:for-each>
                        </tr>
                    </thead>
                    <tbody>
                        <xsl:apply-templates select="//body">
                            <xsl:with-param name="liber">
                                <xsl:value-of select="$liber"/>
                            </xsl:with-param>
                        </xsl:apply-templates>
                    </tbody>
                </table>
            </div>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="text/body">
        <xsl:param name="liber"/>
        <xsl:for-each-group select="child::div/div/div" group-by="@resp">
            <tr>
                <xsl:variable name="resp">
                    <xsl:value-of select="@resp"/>
                </xsl:variable>
                <th>
                    <xsl:choose>
                        <xsl:when
                            test="name(/TEI/teiHeader//*[@xml:id = substring-after($resp, '#')]/..) = 'respStmt'">
                            <xsl:value-of
                                select="/TEI/teiHeader//*[@xml:id = substring-after($resp, '#')]/.."
                            />
                        </xsl:when>
                        <xsl:when
                            test="name(/TEI/teiHeader//*[@xml:id = substring-after($resp, '#')]/..) = 'handDesc'">
                            <xsl:value-of
                                select="/TEI/teiHeader//*[@xml:id = substring-after($resp, '#')]/.."
                            />
                        </xsl:when>
                        <xsl:when
                            test="exists(/TEI/teiHeader/fileDesc/sourceDesc/listPerson[@type = 'commentateurs']/person[@xml:id = substring-after($resp, '#')])">
                            <xsl:value-of
                                select="/TEI/teiHeader/fileDesc/sourceDesc/listPerson[@type = 'commentateurs']/person[@xml:id = substring-after($resp, '#')]/persName"
                            />
                        </xsl:when>
                        <xsl:otherwise>
                            <span>
                                <xsl:attribute name="title">
                                    <xsl:text>WARNING: In </xsl:text>
                                    <xsl:value-of select="base-uri()"/>
                                    <xsl:text> - Value of @resp used in note (id: </xsl:text>
                                    <xsl:value-of select="@xml:id"/>
                                    <xsl:text>) declared nor in respStmt, nor in handDesc: </xsl:text>
                                    <xsl:value-of select="@resp"/>
                                </xsl:attribute>
                                <xsl:value-of select="$resp"/>
                            </span>
                            <xsl:message>
                                <xsl:text>WARNING: In </xsl:text>
                                <xsl:value-of select="base-uri()"/>
                                <xsl:text> - Value of @resp used in note (id: </xsl:text>
                                <xsl:value-of select="@xml:id"/>
                                <xsl:text>) declared nor in respStmt, nor in handDesc: </xsl:text>
                                <xsl:value-of select="$resp"/></xsl:message>
                        </xsl:otherwise>
                    </xsl:choose>
                </th>
                <xsl:for-each
                    select="/TEI/text/front//div[@type = 'libre' and @xml:id = $liber]/div[@type = 'paragraph']/span[@type = 'sub-paragraph']">
                    <xsl:variable name="subparagraph">
                        <xsl:value-of select="@xml:id"/>
                    </xsl:variable>
                    <td>
                        <xsl:for-each
                            select="/TEI/text/body//div[@resp = $resp]/p/ref[substring(@target, 2) = $subparagraph]">
                            <xsl:variable name="ana">
                                <xsl:choose>
                                    <xsl:when test="exists(../../@ana) and (../../@ana != '')">
                                        <xsl:value-of select="../../@ana"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:text>vide</xsl:text>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:variable>
                            <xsl:variable name="comment_id">
                                <xsl:value-of select="../../@xml:id"/>
                            </xsl:variable>
                            <xsl:for-each select="tokenize($ana, ' ')">
                                <xsl:variable name="this_ana_value">
                                    <xsl:choose>
                                        <xsl:when test="contains(., '#')">
                                            <xsl:value-of select="substring-after(., '#')"/>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:value-of select="."/>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </xsl:variable>
                                <!--<xsl:message>
                                    <xsl:value-of select="$this_ana_value"/>
                                    <xsl:text> // </xsl:text>
                                    <xsl:value-of select="$comment_id"/>
                                </xsl:message>-->
                                <xsl:if test="string-length($this_ana_value) > 0">
                                    <a>
                                        <xsl:attribute name="href">
                                            <xsl:text>?page=livre</xsl:text>
                                            <xsl:value-of select="substring($liber, 2)"/>
                                            <xsl:text>#</xsl:text>
                                            <xsl:value-of select="$comment_id"/>
                                        </xsl:attribute>
                                        <xsl:attribute name="data-label" select="$this_ana_value"/>
                                        <xsl:attribute name="class">
                                            <xsl:choose>
                                                <xsl:when
                                                  test="count($typeMap//item[@xml:id = $this_ana_value]) =0">
                                                  <!-- when commentary type is not in the "official list"-->
                                                  <xsl:text>text-muted</xsl:text>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:attribute>
                                        <xsl:attribute name="title">
                                            <xsl:choose>
                                                <xsl:when
                                                  test="count($typeMap//item[@xml:id = $this_ana_value]) >= 1">
                                                  <xsl:value-of
                                                  select="concat(upper-case(substring($typeMap//item[@xml:id = $this_ana_value]/name, 1, 1)), substring($typeMap//item[@xml:id = $this_ana_value]/name, 2))"
                                                  />
                                                </xsl:when>
                                                <xsl:otherwise>
                                                  <!-- default case, when commentary type is not in the "official list"-->
                                                  <xsl:text>À corriger. Valeur actuelle : "</xsl:text>
                                                  <xsl:value-of select="$this_ana_value"/>
                                                  <xsl:text>"</xsl:text>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </xsl:attribute>
                                        <xsl:if test="position() > 1">
                                            <xsl:attribute name="style"
                                                >margin-left:-1ex</xsl:attribute>
                                        </xsl:if>
                                        <xsl:choose>
                                            <xsl:when test="$ana = 'vide'">
                                                <b><xsl:text>◯</xsl:text></b>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:text>⬤</xsl:text>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                        <xsl:if test="position() = last()">
                                            <br/>
                                        </xsl:if>
                                    </a>
                                </xsl:if>
                            </xsl:for-each>
                        </xsl:for-each>
                    </td>
                </xsl:for-each>
            </tr>
        </xsl:for-each-group>
    </xsl:template>

</xsl:stylesheet>
