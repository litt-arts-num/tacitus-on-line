<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tei" version="2.0" xpath-default-namespace="http://www.tei-c.org/ns/1.0">
    
    <xsl:param name="typeMap">
        <mapArgOf name="Moral" value="moral" encoding="mor" theme="info" bootstrap-span-class="text-info" cogitore-description="développement d’une idée morale (pas tjs indépendante d’autre, comme pol)"/>
        <mapArgOf name="Controverse" value="controversy" encoding="controv" theme="warning" bootstrap-span-class="text-warning" cogitore-description="controverse scientifique, discussion avec un de ses devanciers ; accord ou désaccord"/>
        <mapArgOf name="Politique" value="politic" encoding="pol" theme="success" bootstrap-span-class="text-success" cogitore-description="une interprétation politique faite par Lipse sur une institution, ou sur un fait politique;"/>
        <mapArgOf name="Historique" value="historic" encoding="hist" theme="dark" bootstrap-span-class="text-dark" cogitore-description="explication historique ; par exemple sur durée d’une magistrature, sur une date ;"/>
        <mapArgOf name="Vocabulaire" value="vocabulary" encoding="voc" theme="primary" bootstrap-span-class="text-primary" cogitore-description="apport d’un synonyme pour un mot ou une expression"/>
        <mapArgOf name="Style" value="style" encoding="styl" theme="primary" bootstrap-span-class="text-danger" cogitore-description="sur un fait de style, imitation d’un devancier,etc"/>
        <mapArgOf name="Établissement du texte" value="establishment" encoding="etab" theme="danger" bootstrap-span-class="text-danger" cogitore-description="établissement: quand Juste Lipse corrige le texte de Tacite et en propose une lecture différente de ses devanciers ou des manuscrits"/>
        <mapArgOf name="Personnel" value="personal" encoding="pers" theme="default" bootstrap-span-class="text-muted" cogitore-description="pour une intervention personnelle du commentateur, un renvoi à son expérience personnelle, ou connaissance personnelle d’un lieu"/>
        <mapArgOf name="Interprétation" value="interpretation" encoding="interp" theme="default" bootstrap-span-class="text-muted" cogitore-description="pour interpréter le but poursuivi par un personnage"/>
        <mapArgOf name="Renvoi" value="reference" encoding="ref" theme="default" bootstrap-span-class="text-muted" cogitore-description="renvoi à un auteur antique, ou à un écrit , autre  livre du commentateur ou excursus"/>
        <mapArgOf name="Cit. auteur" value="author_citation" encoding="litt" theme="default" bootstrap-span-class="text-muted" cogitore-description="citation d’un auteur antique, latin ou grec ; contient parfois une proposition de correction du texte cité, émendation"/>
        <mapArgOf name="Cit. inscription lapidaire" value="inscription_citation" encoding="epig" theme="default" bootstrap-span-class="text-muted" cogitore-description="citation d’une inscription lapidaire"/>
        <mapArgOf name="Cit. légende monétaire" value="legend_citation" encoding="num" theme="default" bootstrap-span-class="text-muted" cogitore-description="citation d’une légende monétaire"/>
        <mapArgOf name="À corriger" value="a_corriger" encoding="default" theme="default" bootstrap-span-class="text-muted" cogitore-description=""/>
    </xsl:param>
</xsl:stylesheet>