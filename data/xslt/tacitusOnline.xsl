<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs tei" version="2.0"
    xmlns:tei="http://www.tei-c.org/ns/1.0">

    <xsl:output method="html" omit-xml-declaration="yes" indent="yes"/>
    <xsl:strip-space elements="*"/>
    <xsl:param name="typeMap">
        <xsl:copy-of select="//tei:list[@xml:id = 'typology_commentary']"/>
    </xsl:param>
    <xsl:variable name="DEBUG" as="xs:boolean">true</xsl:variable>

    <xsl:template match="tei:TEI">
        <!-- FOREACH LIBER : one output -->
        <xsl:for-each select="//tei:div[@type = 'libre']/@xml:id">
            <xsl:variable name="curent_liber">
                <xsl:value-of select="."/>
            </xsl:variable>
            <xsl:result-document href="../../views/html/liber{substring($curent_liber,2)}.html"
                method="xhtml" indent="yes" omit-xml-declaration="yes">

                <xsl:for-each
                    select="//tei:div[@type = 'libre' and @xml:id = $curent_liber]//tei:div[@type = 'paragraph']">
                    <!-- PARAGRAPH-ID -->
                    <div class="row">
                        <div class="col">
                            <h3>
                                <xsl:attribute name="class">paragraph-id</xsl:attribute>
                                <xsl:attribute name="data-paragraph-id">
                                    <xsl:text>TACITUS_n</xsl:text>
                                    <xsl:value-of select="substring(@xml:id, 4)"/>
                                </xsl:attribute>
                                <xsl:value-of select="substring(@xml:id, 4)"/>
                            </h3>
                        </div>
                    </div>

                    <xsl:for-each select=".//child::tei:span[@type = 'sub-paragraph']">
                        <div class="row">
                            <!-- TEXT -->
                            <!-- SUBPARAGRAPH TACITUS -->
                            <div class="col-4 subparagraph">
                                <xsl:attribute name="id">
                                    <xsl:value-of select="concat('TACITUS_', @xml:id)"/>
                                </xsl:attribute>
                                <xsl:attribute name="data-paragraph-id">
                                    <xsl:value-of
                                        select="substring-before(substring-after(@xml:id, '.'), '.')"
                                    />
                                </xsl:attribute>
                                <xsl:attribute name="data-subparagraph-id">
                                    <xsl:value-of select="substring-after(@xml:id, '.')"/>
                                </xsl:attribute>
                                <span>
                                    <xsl:attribute name="class">subparagraph-title</xsl:attribute>
                                    <xsl:value-of select="position()"/>
                                </span>
                                <!--<xsl:apply-templates mode="highlight_anchor" select=".">
                                    <xsl:with-param name="tacitus_subparagraph_id" select="@xml:id"
                                    />
                                </xsl:apply-templates>-->
                                <xsl:apply-templates select="."/>                                
                            </div>
                            <!-- END TEXT -->
                            <!-- COMMENTS -->
                            <div class="col comments">
                                <!-- COMMENT -->
                                <xsl:variable name="subId">
                                    <xsl:value-of select="substring(@xml:id, 2)"/>
                                </xsl:variable>
                                <xsl:for-each
                                    select="//tei:div/tei:p/tei:ref[substring(@target, 3) = $subId]">
                                    <!-- TYPE OF COMMENTARY -->
                                    <xsl:variable name="data-comment-type">
                                        <xsl:for-each select="parent::tei:p/parent::tei:div">
                                            <xsl:variable name="ana">
                                                <xsl:choose>
                                                  <xsl:when test="exists(@ana) and (@ana != '')">
                                                  <xsl:value-of select="normalize-space(@ana)"/>
                                                  </xsl:when>
                                                  <xsl:otherwise>
                                                  <xsl:text>vide</xsl:text>
                                                  </xsl:otherwise>
                                                </xsl:choose>
                                            </xsl:variable>
                                            <xsl:for-each select="tokenize($ana, ' ')">
                                                <xsl:variable name="typeValue">
                                                  <xsl:choose>
                                                  <xsl:when test="contains(., '#')">
                                                  <xsl:value-of select="substring-after(., '#')"/>
                                                  </xsl:when>
                                                  <xsl:otherwise>
                                                  <xsl:value-of select="."/>
                                                  </xsl:otherwise>
                                                  </xsl:choose>
                                                </xsl:variable>
                                                <xsl:if test="position() > 1">
                                                  <xsl:text> </xsl:text>
                                                </xsl:if>
                                                <xsl:choose>
                                                  <xsl:when
                                                  test="count($typeMap//*[@xml:id = $typeValue]) >= 1">
                                                  <xsl:value-of
                                                  select="$typeMap//*[@xml:id = $typeValue]/@xml:id"
                                                  />
                                                  </xsl:when>
                                                  <xsl:otherwise>
                                                  <!-- default case, when commentary type is not in the "official list"-->
                                                  <xsl:text>default</xsl:text>
                                                  </xsl:otherwise>
                                                </xsl:choose>
                                            </xsl:for-each>
                                        </xsl:for-each>
                                        <!--<xsl:text> </xsl:text>-->
                                    </xsl:variable>

                                    <!-- AUTHOR OF COMMENTARY -->
                                    <xsl:variable name="data-author">
                                        <xsl:value-of
                                            select="substring(parent::tei:p/parent::tei:div/@resp, 2)"
                                        />
                                    </xsl:variable>

                                    <!-- TYPE OF QUOTE WITHIN THE COMMENT -->
                                    <xsl:variable name="data-quote-type">
                                        <xsl:for-each
                                            select="distinct-values(parent::tei:p/tei:q/@ana)">
                                            <xsl:choose>
                                                <xsl:when test="contains(., '#')">
                                                  <xsl:value-of select="substring-after(., '#')"/>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                  <xsl:value-of select="."/>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                            <xsl:choose>
                                                <xsl:when test="position() != last()">
                                                  <xsl:text> </xsl:text>
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:for-each>
                                    </xsl:variable>

                                    <!-- AUTHOR OF QUOTE WITHIN THE COMMENT -->
                                    <xsl:variable name="data-quote-author">
                                        <xsl:for-each
                                            select="distinct-values(parent::tei:p/tei:q/@who)">
                                            <xsl:text> cit_</xsl:text>
                                            <xsl:choose>
                                                <xsl:when test="substring(., 1, 1) = '#'">
                                                  <xsl:value-of select="substring(., 2)"/>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                  <xsl:value-of select="."/>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </xsl:for-each>
                                    </xsl:variable>

                                    <div>
                                        <xsl:attribute name="id">
                                            <xsl:value-of
                                                select="parent::tei:p/parent::tei:div/@xml:id"/>
                                        </xsl:attribute>
                                        <xsl:attribute name="onmouseover">
                                            <xsl:text>highlight_class('</xsl:text>
                                            <xsl:value-of
                                                select="parent::tei:p/parent::tei:div/@xml:id"/>
                                            <xsl:text>')</xsl:text>
                                        </xsl:attribute>
                                        <xsl:attribute name="onmouseleave">
                                            <xsl:text>unhighlight_class('</xsl:text>
                                            <xsl:value-of
                                                select="parent::tei:p/parent::tei:div/@xml:id"/>
                                            <xsl:text>')</xsl:text>
                                        </xsl:attribute>
                                        <xsl:attribute name="class">
                                            <xsl:text>comment card </xsl:text>
                                            <xsl:value-of select="$data-comment-type"/>
                                            <xsl:text> </xsl:text>
                                            <xsl:value-of select="$data-quote-type"/>
                                            <xsl:text> </xsl:text>
                                            <xsl:value-of select="$data-author"/>
                                            <xsl:text> </xsl:text>
                                            <!--<xsl:value-of select="$data-quote-author"/>
                                            <xsl:text> </xsl:text>-->
                                        </xsl:attribute>
                                        <xsl:attribute name="data-comment-type">
                                            <xsl:value-of select="$data-comment-type"/>
                                        </xsl:attribute>
                                        <xsl:attribute name="data-quote-type">
                                            <xsl:value-of select="$data-quote-type"/>
                                        </xsl:attribute>
                                        <xsl:attribute name="data-author">
                                            <xsl:value-of select="$data-author"/>
                                        </xsl:attribute>
                                        <!-- TARGET SUBPARAGRAPH OF TACITUS TEXT -->
                                        <xsl:attribute name="data-target">
                                            <!--<xsl:value-of select="replace($subId, '\.', '_')"/>-->
                                            <xsl:text>TACITUS_n</xsl:text>
                                            <xsl:value-of select="$subId"/>
                                            <xsl:text> </xsl:text>
                                            <xsl:text>TACITUS_n</xsl:text>
                                            <xsl:value-of
                                                select="substring-before(substring-after($subId, '.'), '.')"
                                            />
                                        </xsl:attribute>

                                        <!-- CARD HEADER -->
                                        <div>
                                            <xsl:attribute name="class">
                                                <xsl:text>card-header</xsl:text>
                                            </xsl:attribute>
                                            <xsl:text>&#xa;</xsl:text>
                                            <!-- newline just for nice output  -->
                                            <button>
                                                <xsl:attribute name="class">btn</xsl:attribute>
                                                <xsl:attribute name="data-toggle"
                                                  >collapse</xsl:attribute>
                                                <xsl:attribute name="data-target"
                                                  >#collapse<xsl:value-of select="$subId"
                                                  />_<xsl:value-of select="position()"
                                                  /></xsl:attribute>
                                                <xsl:text>&#xa;</xsl:text>
                                                <!-- newline just for nice output and  -->
                                                <span>
                                                  <xsl:attribute name="class"
                                                  >comment-title</xsl:attribute>
                                                  <span class="badge">
                                                  <xsl:value-of
                                                  select="ancestor::tei:div[1]/@xml:id"/>
                                                  </span>
                                                  <xsl:text> </xsl:text>
                                                  <!-- OK only if commentaries are encoded in the "right order"... not a good idea
                                                  <xsl:if test="not(matches(., '^[1-9]'))">
                                                      <xsl:number select="ancestor::tei:div[1]" count="tei:div[@resp = concat('#',$data-author)]" level="any" format="1"/>
                                                      <xsl:text>. </xsl:text>
                                                  </xsl:if>
                                                  -->
                                                  <xsl:apply-templates mode="lemma"/>
                                                </span>
                                                <xsl:text>&#xa;</xsl:text>
                                                <!-- newline just for nice output  -->
                                            </button>
                                            <xsl:text>&#xa;</xsl:text>
                                            <!-- newline just for nice output  -->
                                            <xsl:variable name="facs"
                                                select="substring-after(parent::tei:p/parent::tei:div/@facs, '#')"/>
                                            <xsl:if
                                                test="exists(/tei:TEI/tei:facsimile/tei:surface/tei:zone[@corresp = $facs])">
                                                <xsl:variable name="this_xml_id">
                                                  <xsl:value-of
                                                  select="/tei:TEI/tei:facsimile/tei:surface/tei:zone[@corresp = $facs][1]/@xml:id"
                                                  />
                                                </xsl:variable>
                                                <button type="button" class="btn btn-light btn-sm"
                                                  data-toggle="modal"
                                                  data-target="#modal{$this_xml_id}">
                                                  <i class="fa fa-eye" aria-hidden="true"/>
                                                </button>
                                                <div class="modal fade" id="modal{$this_xml_id}"
                                                  tabindex="-1" role="dialog"
                                                  aria-labelledby="modal{$this_xml_id}"
                                                  aria-hidden="true">
                                                  <div class="modal-dialog" role="document">
                                                  <div class="modal-content">
                                                  <div class="modal-header">
                                                  <h5 class="modal-title" id="exampleModalLabel">
                                                  <xsl:text>Facsimilé</xsl:text>
                                                  <xsl:if
                                                  test="count(/tei:TEI/tei:facsimile/tei:surface/tei:zone[@corresp = $facs]) > 1">
                                                  <xsl:text>s</xsl:text>
                                                  </xsl:if>
                                                  <xsl:text> du commentaire</xsl:text>
                                                  </h5>
                                                  <button type="button" class="close"
                                                  data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">x</span>
                                                  </button>
                                                  </div>
                                                  <div class="modal-body">
                                                  <xsl:for-each
                                                  select="/tei:TEI/tei:facsimile/tei:surface/tei:zone[@corresp = $facs]">
                                                  <xsl:variable name="x" select="@ulx"
                                                  as="xs:integer"/>
                                                  <xsl:variable name="y" select="@uly"
                                                  as="xs:integer"/>
                                                  <xsl:variable name="xx" select="@lrx"
                                                  as="xs:integer"/>
                                                  <xsl:variable name="yy" select="@lry"
                                                  as="xs:integer"/>
                                                  <div class="float-right">
                                                  <a class="btn btn-light m-0 mt-3 btn-sm"
                                                  href="{preceding-sibling::tei:graphic/@url}full/1000,/0/default.jpg"
                                                  target="_blank">Voir la page dans son ensemble</a>
                                                  <span class="btn m-0 mt-3 btn-sm"> / </span>
                                                  <a class="btn btn-light m-0 mt-3 btn-sm"
                                                  href="{preceding-sibling::tei:graphic/@url}full/full/0/default.jpg"
                                                  target="_blank">Voir en HD</a>
                                                  </div>
                                                  <img class="facs" loading="lazy"
                                                  src="{preceding-sibling::tei:graphic/@url}{$x},{$y},{$xx - $x},{$yy - $y}/400,/0/default.jpg"
                                                  alt="Image indisponible"/>
                                                  </xsl:for-each>
                                                  <span class="source">Source : Bibliothèque
                                                  Municipale de Lyon</span>
                                                  </div>
                                                  </div>
                                                  </div>
                                                </div>

                                            </xsl:if>
                                            <xsl:text>&#xa;</xsl:text>
                                            <!-- newline just for nice output and  -->
                                            <div>
                                                <xsl:attribute name="class"
                                                  >float-right</xsl:attribute>
                                                <xsl:variable name="commentaryId">
                                                  <xsl:value-of
                                                  select="parent::tei:p/parent::tei:div/@xml:id"/>
                                                </xsl:variable>
                                                <xsl:choose>
                                                  <xsl:when
                                                  test="exists(parent::tei:p/parent::tei:div/@ana)">
                                                  <xsl:for-each
                                                  select="tokenize(parent::tei:p/parent::tei:div/@ana, ' ')">
                                                  <xsl:if test=".">
                                                  <xsl:variable name="typeValue">
                                                  <xsl:choose>
                                                  <xsl:when test="contains(., '#')">
                                                  <xsl:value-of select="substring-after(., '#')"/>
                                                  </xsl:when>
                                                  <xsl:otherwise>
                                                  <xsl:value-of select="."/>
                                                  </xsl:otherwise>
                                                  </xsl:choose>
                                                  </xsl:variable>
                                                  <xsl:text>&#xa;</xsl:text>
                                                  <!-- newline just for nice output and  -->
                                                  <span>
                                                  <xsl:attribute name="data-type"
                                                  select="$typeValue"/>
                                                  <xsl:choose>
                                                  <xsl:when
                                                  test="count($typeMap//*[@xml:id = $typeValue]/*) >= 1">
                                                  <xsl:attribute name="class">
                                                  <xsl:text>align-items-center badge</xsl:text>
                                                  </xsl:attribute>
                                                  <xsl:value-of
                                                  select="concat(upper-case(substring($typeMap//*[@xml:id = $typeValue]/tei:name, 1, 1)), substring($typeMap//*[@xml:id = $typeValue]/tei:name, 2))"
                                                  />
                                                  </xsl:when>
                                                  <xsl:otherwise>
                                                  <!-- when commentary type is not in the "official list"-->
                                                  <xsl:attribute name="class">
                                                  <xsl:text>align-items-center badge badge-default</xsl:text>
                                                  </xsl:attribute>
                                                  <xsl:attribute name="title">
                                                  <xsl:text>Type encodé : "</xsl:text>
                                                  <xsl:value-of select="$typeValue"/>
                                                  <xsl:text>" (commentaire </xsl:text>
                                                  <xsl:value-of select="$commentaryId"/>
                                                  <xsl:text>)</xsl:text>
                                                  </xsl:attribute>
                                                  <xsl:text>À corriger</xsl:text>
                                                  </xsl:otherwise>
                                                  </xsl:choose>
                                                  </span>
                                                  <!--<xsl:text>&#160;</xsl:text>-->
                                                  </xsl:if>
                                                  </xsl:for-each>
                                                  </xsl:when>
                                                  <xsl:otherwise>
                                                  <small>
                                                  <small>
                                                  <i>non classé</i>
                                                  </small>
                                                  </small>
                                                  </xsl:otherwise>
                                                </xsl:choose>
                                            </div>
                                        </div>
                                        <!-- END CARD HEADER -->
                                        <div>
                                            <xsl:attribute name="id">collapse<xsl:value-of
                                                  select="$subId"/>_<xsl:value-of
                                                  select="position()"/></xsl:attribute>
                                            <xsl:attribute name="class">collapse
                                                comment_content</xsl:attribute>
                                            <div>
                                                <xsl:attribute name="class"
                                                  >card-body</xsl:attribute>

                                                <xsl:apply-templates
                                                  select="parent::tei:p/node() except (parent::tei:p/tei:ref)"
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </xsl:for-each>
                            </div>
                        </div>
                    </xsl:for-each>
                </xsl:for-each>
            </xsl:result-document>
        </xsl:for-each>
        <!-- END FOREACH LIBER : one output -->
    </xsl:template>

    <xsl:template name="removeBelleLetreNumbers" match="text()">
        <xsl:value-of select="replace(., '\{[^{]*\}', '')"/>
    </xsl:template>

    <xsl:template match="tei:q[count(tei:l) = 1]">
        <span style="display:block;"/>
        "<span>
            <xsl:attribute name="id">
                <xsl:value-of select="@xml:id"/>
            </xsl:attribute>
            <xsl:attribute name="title">
                <xsl:text>Auteur cité : </xsl:text>
                <xsl:value-of select="@who"/>
            </xsl:attribute>
            <xsl:attribute name="data-type">
                <xsl:choose>
                    <xsl:when test="contains(@ana, '#')">
                        <xsl:value-of select="substring-after(@ana, '#')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="@ana"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
            <xsl:attribute name="data-author">
                <xsl:for-each select="tokenize(@who, '\s+')">
                    <xsl:value-of select="substring(., 2)"/>
                    <xsl:choose>
                        <xsl:when test="position() != last()">
                            <xsl:text> </xsl:text>
                        </xsl:when>
                    </xsl:choose>
                </xsl:for-each>
            </xsl:attribute>
            <xsl:apply-templates mode="inline"/>
        </span>"
        <span style="display:block;"/>
    </xsl:template>

    <xsl:template match="tei:q">
        "<span>
            <xsl:attribute name="id">
                <xsl:value-of select="@xml:id"/>
            </xsl:attribute>
            <xsl:attribute name="title">
                <xsl:text>Auteur cité : </xsl:text>
                <xsl:value-of select="@who"/>
            </xsl:attribute>
            <xsl:attribute name="data-type">
                <xsl:choose>
                    <xsl:when test="contains(@ana, '#')">
                        <xsl:value-of select="substring-after(@ana, '#')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="@ana"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
            <xsl:attribute name="data-author">
                <xsl:for-each select="tokenize(@who, '\s+')">
                    <xsl:value-of select="substring(., 2)"/>
                    <xsl:choose>
                        <xsl:when test="position() != last()">
                            <xsl:text> </xsl:text>
                        </xsl:when>
                    </xsl:choose>
                </xsl:for-each>
            </xsl:attribute>
            <xsl:apply-templates/>
        </span>"
    </xsl:template>

    <xsl:template match="tei:span">
        <span>
            <xsl:attribute name="data-analyse">
                <xsl:choose>
                    <xsl:when test="contains(., '#')">
                        <xsl:value-of select="substring-after(., '#')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="."/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="tei:choice">
        <correction>
            <xsl:attribute name="title">
                <xsl:text>Correction de l'éditeur de la forme "</xsl:text>
                <xsl:value-of select="tei:orig"/>
            </xsl:attribute>
            <xsl:value-of select="tei:reg"/>
        </correction>
    </xsl:template>

    <xsl:template match="tei:lg">
        <span class="lg" title="Groupe de vers">
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template match="tei:l">
        <xsl:choose>
            <xsl:when test="exists(../tei:lg)">
                <xsl:apply-templates/>
                <br/>
            </xsl:when>
            <xsl:otherwise>
                <span class="l" title="Vers">
                    <xsl:apply-templates/>
                </span>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="tei:l" mode="inline">
        <span title="Vers">
            <xsl:apply-templates/>
        </span>
    </xsl:template>

    <xsl:template mode="lemma" match="tei:num"/>

    <xsl:template match="tei:span" mode="highlight_anchor">
        <xsl:param name="tacitus_subparagraph_id"/>
        <xsl:variable name="tacitus_subparagraph" select="replace(., '\{[^{]*\}', '')"/>
        <!-- remove Belles Lettres numbers -->
        <xsl:variable name="search">
            <xsl:for-each
                select="/tei:TEI/tei:text/tei:body//tei:div[@resp]/tei:p/tei:ref[@target = concat('#', $tacitus_subparagraph_id)]/text()">
                <xsl:variable name="anchor_txt">
                    <xsl:choose>
                        <xsl:when test="exists(@corresp) and @corresp != ''">
                            <xsl:value-of select="@corresp"/>
                        </xsl:when>
                        <!-- si commence par "###." (nombre.) et fini par ".]" -->
                        <xsl:when test="matches(., '^[0-9]+\.') and matches(., '\.\] ?$')">
                            <xsl:value-of select="substring-before(substring-after(., '.'), '.]')"/>
                        </xsl:when>
                        <!-- si commence par "###." (nombre.) et fini par "]" -->
                        <xsl:when test="matches(., '^[0-9]+\.') and matches(., '\] ?$')">
                            <xsl:value-of select="substring-before(substring-after(., '.'), ']')"/>
                            <!--
                                select="normalize-space(translate(lower-case(substring-before(substring-after(., '.'), ']')), 'v', 'u'))"
                                -->
                        </xsl:when>
                        <!-- si fini par ".]" -->
                        <xsl:when test="matches(., '\.\] ?$')">
                            <xsl:value-of
                                select="normalize-space(translate(lower-case(substring-before(., '.]')), 'v', 'u'))"
                            />
                        </xsl:when>
                        <!-- si fini par "]" -->
                        <xsl:when test="matches(., '\] ?$')">
                            <xsl:value-of
                                select="normalize-space(translate(lower-case(substring-before(., ']')), 'v', 'u'))"
                            />
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:if test="$DEBUG">
                                <xsl:message>
                                    <xsl:text>Anchor seems strange (no final "]")... "</xsl:text>
                                    <xsl:value-of select="."/>
                                    <xsl:text>" (in </xsl:text>
                                    <xsl:value-of select="../../@xml:id"/>
                                    <xsl:text>)</xsl:text>
                                </xsl:message>
                            </xsl:if>
                            <xsl:choose>
                                <xsl:when test="matches(., '^[0-9]+\.')">
                                    <xsl:value-of
                                        select="normalize-space(translate(lower-case(substring-after(., '.')), 'v', 'u'))"
                                    />
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of
                                        select="normalize-space(translate(lower-case(.), 'v', 'u'))"
                                    />
                                </xsl:otherwise>
                            </xsl:choose>
                            <xsl:text>,</xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <!--<xsl:message>Anchor is: <xsl:value-of select="$anchor_txt"/></xsl:message>-->
                <xsl:variable name="commentary_id" select="../../@xml:id"/>
                <xsl:choose>
                    <xsl:when test="$anchor_txt != ''">
                        <xsl:choose>
                            <!--select="normalize-space(translate(lower-case(substring-before(substring-after(., '.'), ']')), 'v', 'u'))"-->
                            <xsl:when
                                test="not(matches($tacitus_subparagraph, normalize-space(translate(lower-case($anchor_txt), 'v', 'u'))))">
                                <!--<xsl:message>
                                    <xsl:text>Searching for "</xsl:text>
                                    <xsl:value-of select="$anchor_txt"/>
                                    <xsl:text>" in </xsl:text>
                                    <xsl:value-of select="$tacitus_subparagraph_id"/>
                                    <xsl:text> (</xsl:text>
                                    <xsl:value-of select="@xml:id"/>
                                    <xsl:text>)</xsl:text>
                                </xsl:message>-->
                            </xsl:when>
                            <xsl:otherwise>
                                <!--<xsl:value-of select="$anchor_txt"/>
                                <xsl:text>,</xsl:text>-->
                                <!--<xsl:message>Youpi !</xsl:message>-->
                            </xsl:otherwise>
                        </xsl:choose>
                        <xsl:value-of select="$anchor_txt"/>
                        <xsl:text>,</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:message>Anchor is empty (<xsl:value-of select="@xml:id"
                            />)</xsl:message>
                    </xsl:otherwise>
                </xsl:choose>
                <!--<xsl:value-of select="$anchor_txt"/>
                <xsl:text>,</xsl:text>-->
            </xsl:for-each>
        </xsl:variable>
        <xsl:variable name="replace">
            <!-- TODO: Should be simplified and reuse search instead of recalculating everything -->
            <xsl:for-each
                select="/tei:TEI/tei:text/tei:body//tei:div[@resp]/tei:p/tei:ref[@target = concat('#', $tacitus_subparagraph_id)]">
                <xsl:variable name="anchor_txt">
                    <xsl:choose>
                        <!-- si commence par "###." (nombre.) et fini par ".]" -->
                        <xsl:when test="matches(., '^[0-9]+\.') and matches(., '\.\] ?$')">
                            <xsl:value-of select="substring-before(substring-after(., '.'), '.]')"/>
                        </xsl:when>
                        <!-- si commence par "###." (nombre.) et fini par "]" -->
                        <xsl:when test="matches(., '^[0-9]+\.') and matches(., '\] ?$')">
                            <xsl:value-of select="substring-before(substring-after(., '.'), ']')"/>
                        </xsl:when>
                        <!-- si fini par ".]" -->
                        <xsl:when test="matches(., '\.\] ?$')">
                            <xsl:value-of select="substring-before(., '.]')"/>
                        </xsl:when>
                        <!-- si fini par "]" -->
                        <xsl:when test="matches(., '\] ?$')">
                            <xsl:value-of select="substring-before(., ']')"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <!--<xsl:message>
                                <xsl:text>Anchor seems strange... "</xsl:text>
                                <xsl:value-of select="."/>
                                <xsl:text>" (in </xsl:text>
                                <xsl:value-of select="../../@xml:id"/>
                                <xsl:text>)</xsl:text>
                            </xsl:message>-->
                            <xsl:choose>
                                <xsl:when test="matches(., '^[0-9]+\.')">
                                    <xsl:value-of select="substring-after(., '.')"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="."/>
                                </xsl:otherwise>
                            </xsl:choose>
                            <xsl:text>,</xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:variable name="normalized_anchor_txt">
                    <xsl:value-of
                        select="normalize-space(translate(lower-case($anchor_txt), 'v', 'u'))"/>
                </xsl:variable>
                <xsl:variable name="commentary_id" select="../../@xml:id"/>
                <xsl:choose>
                    <xsl:when test="$normalized_anchor_txt != ''">
                        <xsl:choose>
                            <!-- Attention si l'indentation provoque un saut de ligne entre le crochet ] du lemme
                                 et la balise fermante </ref>, erreur à la transformation :
                                   Syntax error at char 50 in regular expression: Unexpected closing ']'
                                 Solution : rechercher "]
 *</ref>" et corriger
                            -->
                            <!-- normalize-space(translate(lower-case(substring-before(substring-after(., '.'), ']')), 'v', 'u')) -->
                            <xsl:when
                                test="not(matches($tacitus_subparagraph, $normalized_anchor_txt))">
                                <xsl:text>&lt;mark onmouseover="highlight('</xsl:text>
                                <xsl:value-of select="$commentary_id"/>
                                <xsl:text>')" onmouseleave="unhighlight('</xsl:text>
                                <xsl:value-of select="$commentary_id"/>
                                <xsl:text>')" class="commented_span </xsl:text>
                                <xsl:value-of select="$commentary_id"/>
                                <xsl:text>"></xsl:text>
                                <xsl:variable name="start">
                                    <xsl:value-of
                                        select="xs:double(string-length(substring-before(lower-case(translate($tacitus_subparagraph, 'v', 'u')), $normalized_anchor_txt)))"
                                    />
                                </xsl:variable>
                                <xsl:variable name="length">
                                    <xsl:value-of
                                        select="xs:double(string-length($normalized_anchor_txt)) + 1"
                                    />
                                </xsl:variable>
                                <xsl:value-of
                                    select="substring($tacitus_subparagraph, $start, $length)"/>
                                <xsl:text>&lt;/mark></xsl:text>
                                <xsl:text>,</xsl:text>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:variable name="start">
                                    <xsl:value-of
                                        select="xs:double(string-length(substring-before($tacitus_subparagraph, $normalized_anchor_txt)))"
                                    />
                                </xsl:variable>
                                <xsl:variable name="length">
                                    <xsl:value-of
                                        select="xs:double(string-length($normalized_anchor_txt)) + 1"
                                    />
                                </xsl:variable>
                                <xsl:text>&lt;mark onmouseover="highlight('</xsl:text>
                                <xsl:value-of select="$commentary_id"/>
                                <xsl:text>')" onmouseleave="unhighlight('</xsl:text>
                                <xsl:value-of select="$commentary_id"/>
                                <xsl:text>')" class="commented_span </xsl:text>
                                <xsl:value-of select="$commentary_id"/>
                                <xsl:text>"></xsl:text>
                                <xsl:value-of
                                    select="substring($tacitus_subparagraph, $start, $length)"/>
                                <xsl:text>&lt;/mark></xsl:text>
                                <xsl:text>,</xsl:text>
                                <!--<xsl:message>Youpi !</xsl:message>-->
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:message>Anchor is empty (<xsl:value-of select="@xml:id"
                            />)</xsl:message>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
        </xsl:variable>

        <xsl:if test="$DEBUG">
            <xsl:message>[[<xsl:value-of select="$search"/>]]=[[<xsl:value-of select="$replace"
                />]]</xsl:message>
        </xsl:if>
        <xsl:variable name="out">
            <!--<xsl:message> Init search "<xsl:value-of select="$search"/>" </xsl:message>-->
            <xsl:call-template name="string-replace-all">
                <xsl:with-param name="text" select="normalize-space($tacitus_subparagraph)"/>
                <xsl:with-param name="normalized_text"
                    select="translate(lower-case(normalize-space($tacitus_subparagraph)), 'v', 'u')"/>
                <xsl:with-param name="replace" select="$search"/>
                <xsl:with-param name="by" select="$replace"/>
                <xsl:with-param name="times" select="1"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:value-of select="$out" disable-output-escaping="yes"/>
    </xsl:template>

    <!--Author: Ibrahim Naji - Modifed by AnneGF (04/2020)-->
    <!-- Replace substring function - First occurrence only -->
    <xsl:template name="string-replace-all">
        <xsl:param name="text"/>
        <xsl:param name="normalized_text"/>
        <xsl:param name="replace"/>
        <xsl:param name="by"/>
        <xsl:param name="times"/>
        <!--<xsl:if test="15 >= $times">-->

        <xsl:if test="$DEBUG">
            <xsl:message>normalized text: "<xsl:value-of select="$normalized_text"/>"</xsl:message>
            <xsl:message>replace: "<xsl:value-of select="substring-before($replace, ',')"
                />"</xsl:message>
            <xsl:message>by: "<xsl:value-of select="substring-before($by, ',')"/>"</xsl:message>
            <xsl:message>in: "<xsl:value-of select="$text"/>"</xsl:message>
            <xsl:message>times: "<xsl:value-of select="$times"/>"</xsl:message>
            <xsl:message>remaining replace: <xsl:value-of select="substring-after($replace, ',')"
                /></xsl:message>
        </xsl:if>
        <xsl:choose>
            <!--Checks if $text contains the first replace string in the $replace value-->
            <xsl:when
                test="$replace != '' and contains($normalized_text, substring-before($replace, ','))">
                <xsl:variable name="pos"
                    select="string-length(substring-before($normalized_text, substring-before($replace, ',')))"/>
                <xsl:if test="$DEBUG">
                    <xsl:message>Found it! Position = <xsl:value-of select="$pos"/></xsl:message>
                </xsl:if>
                <xsl:call-template name="string-replace-all">
                    <xsl:with-param name="text" select="
                            concat(
                            substring($text, 1, $pos),
                            substring-before($by, ','),
                            substring($text, $pos + string-length(substring-before($replace, ',')) + 1)
                            )"/>
                    <xsl:with-param name="normalized_text" select="
                            concat(
                            substring($normalized_text, 1, $pos),
                            substring-before($by, ','),
                            substring($normalized_text, $pos + string-length(substring-before($replace, ',')) + 1)
                            )"/>
                    <xsl:with-param name="replace" select="substring-after($replace, ',')"/>
                    <xsl:with-param name="by" select="substring-after($by, ',')"/>
                    <xsl:with-param name="times" select="$times + 1"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:choose>
                    <!--Checks to see if there is any strings left to replace-->
                    <xsl:when test="substring-after($replace, ',') != ''">
                        <xsl:choose>
                            <!--Here you could call the template directly and the iteration will do the check to see if the next replace string is contained in the $text value-->
                            <!--but to save some iteration loops I did the work here-->
                            <xsl:when
                                test="contains($normalized_text, substring-before(substring-after($replace, ','), ','))">
                                <xsl:call-template name="string-replace-all">
                                    <xsl:with-param name="text" select="$text"/>
                                    <xsl:with-param name="normalized_text" select="$normalized_text"/>
                                    <xsl:with-param name="replace"
                                        select="substring-after($replace, ',')"/>
                                    <xsl:with-param name="by" select="substring-after($by, ',')"/>
                                    <xsl:with-param name="times" select="$times + 1"/>
                                </xsl:call-template>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:call-template name="string-replace-all">
                                    <xsl:with-param name="text" select="$text"/>
                                    <xsl:with-param name="normalized_text" select="$normalized_text"/>
                                    <xsl:with-param name="replace"
                                        select="substring-after($replace, ',')"/>
                                    <xsl:with-param name="by" select="substring-after($by, ',')"/>
                                    <xsl:with-param name="times" select="$times + 1"/>
                                </xsl:call-template>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="$text"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>
        <!--</xsl:if>-->
    </xsl:template>
</xsl:stylesheet>
