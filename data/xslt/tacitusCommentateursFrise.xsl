<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE xml[
<!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tei" version="2.0">
    <xsl:output method="html" omit-xml-declaration="yes" indent="yes"/>

    <xsl:param name="typeMap">
        <xsl:copy-of select="//tei:list[@xml:id = 'typology_commentary']"/>
    </xsl:param>

    <xsl:template match="/tei:TEI">
        <div id="legend">
            <h4>Mode d'emploi et légende</h4>
            <p>
                <span class="lead">Qui a vécu quand ?</span>
                 Ce petit graphique permet de positionner dans le temps les commentateurs à Tacite
                sur lesquels nous avons travaillé.</p>
        </div>
        <div class="progress">
            <span class="col-3 bg-light"/>
            <div class="progress-bar bg-light text-secondary text-left" role="progressbar"
                style="width: 25%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">|
                1450</div>
            <div class="progress-bar bg-light text-secondary text-left" role="progressbar"
                style="width: 25%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">|
                1500</div>
            <div class="progress-bar bg-light text-secondary text-left" role="progressbar"
                style="width: 25%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">|
                1550</div>
            <div class="progress-bar bg-light text-secondary text-left" role="progressbar"
                style="width: 10%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">|
                1600</div>
            <div class="progress-bar bg-light text-secondary text-right" role="progressbar"
                style="width: 15%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">1660
                |</div>
        </div>
        <xsl:for-each select="//tei:listPerson[@type = 'commentateurs']/tei:person">
            <xsl:sort data-type="text"/>
            <xsl:choose>
                <xsl:when test="count(tei:birth) = 1 and count(tei:death) = 1">
                    <div class="progress mt-1">
                        <label class="progress-label col-3 text-left align-middle mr-1">
                            <xsl:for-each select="tei:persName">
                                <xsl:choose>
                                    <xsl:when test="position() = 2">
                                        <xsl:text> (</xsl:text>
                                    </xsl:when>
                                    <xsl:when test="position() > 2">
                                        <xsl:text>, </xsl:text>
                                    </xsl:when>
                                </xsl:choose>
                                <xsl:value-of select="."/>
                                <xsl:choose>
                                    <xsl:when test="last() and position() > 1">
                                        <xsl:text>)</xsl:text>
                                    </xsl:when>
                                </xsl:choose>
                            </xsl:for-each>
                        </label>
                        <xsl:call-template name="make_progress_bar">
                            <xsl:with-param name="num">1</xsl:with-param>
                        </xsl:call-template>
                    </div>
                </xsl:when>
                <xsl:when test="count(tei:birth) > 1 and count(tei:death) > 1">
                    <div class="progress mt-1">
                        <label class="progress-label col-3 text-left align-middle mr-1">
                            ? <xsl:value-of select="tei:persName[1]"/>
                        </label>
                        <xsl:call-template name="make_progress_bar">
                            <xsl:with-param name="num">1</xsl:with-param>
                            <xsl:with-param name="class">progress-bar-striped</xsl:with-param>
                        </xsl:call-template>
                    </div>
                    <div class="progress mt-1">
                        <label class="progress-label col-3 text-left align-middle mr-1">
                            ? <xsl:value-of select="tei:persName[2]"/>
                        </label>
                        <xsl:call-template name="make_progress_bar">
                            <xsl:with-param name="num">2</xsl:with-param>
                            <xsl:with-param name="class">progress-bar-striped</xsl:with-param>
                        </xsl:call-template>
                    </div>
                </xsl:when>
                <xsl:otherwise>
                    <div class="progress mt-1">
                        <label class="progress-label col-3 text-left align-middle mr-1">
                            <xsl:for-each select="tei:persName">
                                <xsl:choose>
                                    <xsl:when test="position() = 2">
                                        <xsl:text> (</xsl:text>
                                    </xsl:when>
                                    <xsl:when test="position() > 2">
                                        <xsl:text>, </xsl:text>
                                    </xsl:when>
                                </xsl:choose>
                                <xsl:value-of select="."/>
                                <xsl:choose>
                                    <xsl:when test="last() and position() > 1">
                                        <xsl:text>)</xsl:text>
                                    </xsl:when>
                                </xsl:choose>
                            </xsl:for-each>
                        </label>
                        <div class="progress-bar bg-light text-secondary text-left"
                            role="progressbar" style="width: 50%" aria-valuenow="15"
                            aria-valuemin="0" aria-valuemax="100">
                            <xsl:value-of select="tei:birth"/>
                        </div>
                        <div class="progress-bar bg-light text-secondary text-right"
                            role="progressbar" style="width: 50%" aria-valuenow="15"
                            aria-valuemin="0" aria-valuemax="100">
                            <xsl:value-of select="tei:death"/>
                        </div>
                    </div>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </xsl:template>


    <xsl:template name="make_progress_bar">
        <xsl:param name="num"/>
        <xsl:param name="class"/>
        <xsl:variable name="birth_year">
            <xsl:analyze-string select="normalize-space(tei:birth[position() = $num])"
                regex="(\d{{4}})$">
                <xsl:matching-substring>
                    <xsl:value-of select="regex-group(1)"/>
                </xsl:matching-substring>
                <xsl:non-matching-substring>
                    <xsl:text>0</xsl:text>
                </xsl:non-matching-substring>
            </xsl:analyze-string>
        </xsl:variable>
        <xsl:variable name="death_year">
            <xsl:analyze-string select="normalize-space(tei:death[position() = $num])"
                regex="(\d{{4}})$">
                <xsl:matching-substring>
                    <xsl:value-of select="regex-group(1)"/>
                </xsl:matching-substring>
                <xsl:non-matching-substring>
                    <xsl:text>0</xsl:text>
                </xsl:non-matching-substring>
            </xsl:analyze-string>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="xs:integer($birth_year) != 0">
                <div class="progress-bar {$class} bg-light text-secondary text-left" role="progressbar"
                    style="width: {(xs:integer($birth_year)-1450) div 2}%" aria-valuenow="15"
                    aria-valuemin="0" aria-valuemax="100"/>
                <div title="{tei:persName[position() = $num]}" class="progress-bar {$class} bg-success text-left"
                    role="progressbar" style="width: 5%" aria-valuenow="30" aria-valuemin="0"
                    aria-valuemax="100">
                    <xsl:text>&nbsp;</xsl:text>
                    <xsl:value-of select="xs:integer($birth_year)"/>
                </div>
                <div title="{tei:persName[position() = $num]}" class="progress-bar {$class} bg-success" role="progressbar"
                    style="width: {((xs:integer($death_year)-xs:integer($birth_year)) div 2)- 10}%"
                    aria-valuenow="4" aria-valuemin="0" aria-valuemax="100"/>
                <div title="{tei:persName[position() = $num]}" class="progress-bar {$class} bg-success text-right"
                    role="progressbar" style="width: 5%" aria-valuenow="30" aria-valuemin="0"
                    aria-valuemax="100">
                    <xsl:value-of select="xs:integer($death_year)"/>
                    <xsl:text>&nbsp;</xsl:text>
                </div>
                <div class="{$class} bg-light text-secondary text-right" role="progressbar"
                    style="width: {(1660 - xs:integer($death_year)) div 2}%" aria-valuenow="20"
                    aria-valuemin="0" aria-valuemax="100"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="tei:birth"/> - <xsl:value-of select="tei:death"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
</xsl:stylesheet>
