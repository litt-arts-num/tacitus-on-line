<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tei" version="2.0"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0">
    <xsl:output method="html" omit-xml-declaration="yes" indent="yes"/>


    <xsl:template match="TEI">
        <!-- generate "quoteAuthors" filters -->
        <xsl:result-document href="../../views/html/quoteAuthors.html" method="xhtml" indent="yes"
            omit-xml-declaration="yes">
            <style>
                /*.reset-styles{
                background-color:inherit !important;
                }*/
                .hide {
                    display: none;
                }</style>
            <h4>Mode d'emploi et légende</h4>
            <p>
                <span class="lead">À côté de chaque auteur</span>
                 le nombre de fois où il est cité est indiqué.<br/>
            </p>
            <p>
                <span class="lead">Pourquoi y a-t-il du rouge ?!</span>
                 Un auteur cité est représenté en rouge s'il n'est pas défini. C'est le cas par
                exemple des citations qui font référence à un id non déclaré. Pour déclarer un
                auteur, il faut simplement indiquer au sein du xml:id sa définition en utilisant
                l'un des encodages suivants. Les informations en gras sont obligatoires : les
                informations en italiques sont conseillées : <ul>
                    <li><code>&lt;author <b>xml:id="tacitus"</b>
                            <i>rendition="info"</i>><b>&lt;persName>Tacite&lt;/persName></b>&lt;/author></code></li>
                    <li><code>&lt;person <b>xml:id="ammianus"</b>
                            <i>rendition="warning"</i>> <b>&lt;persName>Ammianus
                                Marcellinus&lt;/persName></b> &lt;birth>vers 330 à Antioche sur
                            l'Oronte&lt;/birth> &lt;death>vers 395 (au plus tard en 400)
                            probablement à Rome&lt;/death> &lt;note>Blabla à propos de
                            l'auteur.&lt;/note> &lt;/person></code></li>
                </ul>
            </p>
            <xsl:for-each-group select="//q" group-by="@who">
                <xsl:sort order="ascending" select="current-grouping-key()"/>
                <xsl:variable name="id">
                    <xsl:choose>
                        <xsl:when test="substring(@who, 1, 1) = '#'">
                            <xsl:value-of select="substring(@who, 2)"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="@who"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:variable name="rendition">
                    <xsl:choose>
                        <xsl:when
                            test="substring(@who, 1, 1) = '#' and exists(//*[@xml:id = $id]/@rendition)">
                            <xsl:value-of select="//*[@xml:id = $id]/@rendition"/>
                        </xsl:when>
                        <xsl:when test="substring(@who, 1, 1) = '#' and exists(//*[@xml:id = $id])">
                            <xsl:text>secondary</xsl:text>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>danger</xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:variable name="persName">
                    <xsl:choose>
                        <xsl:when test="substring(@who, 1, 1) = '#' and exists(//*[@xml:id = $id])">
                            <xsl:value-of
                                select="concat(upper-case(substring(//*[@xml:id = $id]/persName[1], 1, 1)), substring(//*[@xml:id = $id]/persName[1], 2))"
                            />
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="@who"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <button type="button" data-criteria-type="citOf">
                    <xsl:attribute name="id">
                        <xsl:value-of select="$id"/>
                    </xsl:attribute>
                    <xsl:attribute name="title">
                        <xsl:text>Identifiant #</xsl:text>
                        <xsl:value-of select="$id"/>
                    </xsl:attribute>
                    <xsl:attribute name="data-theme">
                        <xsl:value-of select="$rendition"/>
                    </xsl:attribute>
                    <xsl:attribute name="class">
                        <xsl:text>p-0 pr-1 pl-1 mb-1 btn btn-</xsl:text>
                        <xsl:value-of select="$rendition"/>
                    </xsl:attribute>
                    <xsl:attribute name="href">
                        <xsl:text>?page=livre1&amp;filters=cit_</xsl:text>
                        <xsl:value-of select="$id"/>
                        <xsl:text>&amp;op=and</xsl:text>
                    </xsl:attribute>
                    <xsl:value-of select="$persName"/>
                    <span class="badge badge-pill badge-light">
                        <xsl:value-of select="count(current-group())"/>
                    </span>
                </button>
            </xsl:for-each-group>
        </xsl:result-document>

        <!-- generate "quoteByAuthor" filters -->
        <xsl:result-document href="../../views/html/quoteByAuthor.html" method="xhtml" indent="yes"
            omit-xml-declaration="yes">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <label class="input-group-text" for="inputGroupSelectQuoteAuthor">Auteur
                        cité</label>
                </div>
                <select class="custom-select" id="inputGroupSelectQuoteAuthor">
                    <option selected="selected">Choose...</option>
                    <xsl:for-each-group select="//q" group-by="@who">
                        <xsl:sort order="ascending" select="current-grouping-key()"/>
                        <xsl:variable name="id">
                            <xsl:choose>
                                <xsl:when test="substring(@who, 1, 1) = '#'">
                                    <xsl:value-of select="substring(@who, 2)"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="@who"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:variable>
                        <xsl:variable name="persName">
                            <xsl:choose>
                                <xsl:when
                                    test="substring(@who, 1, 1) = '#' and exists(//*[@xml:id = $id])">
                                    <xsl:value-of
                                        select="concat(upper-case(substring(//*[@xml:id = $id]/persName[1], 1, 1)), substring(//*[@xml:id = $id]/persName[1], 2))"
                                    />
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="@who"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:variable>
                        <option>
                            <xsl:attribute name="value">
                                <xsl:value-of select="replace(replace(replace(replace($id,'\?','\\?'),' ','\\ '),'#','\\#'),'\.', '_')"/>
                            </xsl:attribute>
                            <xsl:value-of select="$persName"/>
                        </option>
                    </xsl:for-each-group>
                </select>
            </div>
            <table id="quoteByAuthor" class="output table table-striped table-bordered table-hover table-sm">
                <thead>
                    <tr class="small">
                        <th class="th-sm align-middle">Auteur</th>
                        <th class="th-sm align-middle">Commentateur</th>
                        <th class="th-sm align-middle">Commentaire</th>
                        <th class="th-sm align-middle">§commenté</th>
                        <th class="th-sm align-middle">Type de citation</th>
                        <th class="th-sm align-middle">Citation</th>
                    </tr>
                </thead>
                <tbody>
                    <xsl:for-each select="//q">
                        <xsl:variable name="id">
                            <xsl:choose>
                                <xsl:when test="substring(@who, 1, 1) = '#'">
                                    <xsl:value-of select="substring(@who, 2)"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="@who"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:variable>
                        <xsl:variable name="persName">
                            <xsl:choose>
                                <xsl:when
                                    test="substring(@who, 1, 1) = '#' and exists(//*[@xml:id = $id])">
                                    <xsl:value-of
                                        select="concat(upper-case(substring(//*[@xml:id = $id]/persName[1], 1, 1)), substring(//*[@xml:id = $id]/persName[1], 2))"
                                    />
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="@who"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:variable>
                        <tr>
                            <xsl:attribute name="class">
                                <xsl:text/>
                            </xsl:attribute>
                            <xsl:attribute name="data-quote-author">
                                <xsl:value-of select="replace($id,'\.', '_')"/>
                            </xsl:attribute>
                            <xsl:attribute name="id">
                                <xsl:value-of select="@xml:id"/>
                            </xsl:attribute>
                            <td data-info="quoteAuthor">
                                <xsl:value-of select="$persName"/>
                            </td>
                            <td data-info="commentator">
                                <xsl:value-of select="../../@resp"/>
                            </td>
                            <td data-info="comment-id">
                                <a>
                                    <xsl:attribute name="href">
                                        <xsl:text>?page=livre</xsl:text>
                                        <xsl:value-of select="substring(substring-after(../../@xml:id,'_'),1,1)"/>
                                        <xsl:text>#</xsl:text>
                                        <xsl:value-of select="../../@xml:id"/>
                                    </xsl:attribute>
                                    <xsl:value-of select="../../@xml:id"/>
                                </a>
                            </td>
                            <td data-info="comment-target">
                                <xsl:if test="exists(../ref/@target)">
                                <a>
                                    <xsl:attribute name="href">
                                        <xsl:text>?page=livre</xsl:text>
                                        <xsl:value-of select="substring(substring-after(../../@xml:id,'_'),1,1)"/>
                                        <xsl:text>#TACITUS_</xsl:text>
                                        <xsl:value-of select="substring-after(../ref[1]/@target,'#')"/>
                                    </xsl:attribute>
                                    <xsl:text>Livre </xsl:text>
                                    <xsl:value-of select="substring(substring-after(../../@xml:id,'_'),1,1)"/>
                                    <xsl:text> §</xsl:text><xsl:value-of select="substring(substring-after(../ref[1]/@target,'#'),4)"/>
                                </a>
                                </xsl:if>
                            </td>
                            <td data-info="comment-type">
                                <xsl:value-of select="@ana"/>
                            </td>
                            <td data-info="comment-content">
                                    <xsl:apply-templates select="."/>
                            </td>
                        </tr>
                    </xsl:for-each>
                </tbody>
            </table>
            <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js"/>
            <script>
                /*$(function() {
                $('#inputGroupSelectQuoteAuthor').change(function(){
                $('.quoteAuthor')..addClass("hide");
                $('#' + $(this).val()).removeClass("hide");
                });
                });*/
                $("select#inputGroupSelectQuoteAuthor").change(function() {
                var id = $(this).find(":selected").attr('value');
                $(document.getElementById("quoteByAuthor").querySelectorAll("[data-quote-author]")).addClass("hide");
                var matches = document.querySelectorAll("[data-quote-author="+id+"]");
                $(matches).removeClass("hide");
                });
            </script>

        </xsl:result-document>

    </xsl:template>
    
    <xsl:template match="tei:q">
        <q>
            <xsl:attribute name="id">
                <xsl:value-of select="@xml:id"/>
            </xsl:attribute>
            <xsl:attribute name="data-type">
                <xsl:choose>
                    <xsl:when test="contains(@ana, '#')">
                        <xsl:value-of select="substring-after(@ana, '#')"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="@ana"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
            <xsl:attribute name="data-author">
                <xsl:for-each select="tokenize(@who, '\s+')">
                    <xsl:choose>
                        <xsl:when test="substring(., 1, 1) = '#'">
                            <xsl:value-of select="substring(., 2)"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="."/>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:choose>
                        <xsl:when test="position() != last()">
                            <xsl:text> </xsl:text>
                        </xsl:when>
                    </xsl:choose>
                </xsl:for-each>
            </xsl:attribute>
            <xsl:apply-templates/>
        </q>
    </xsl:template>

    <xsl:template match="tei:choice">
        <correction>
            <xsl:attribute name="title">
                <xsl:text>Correction de l'éditeur de la forme "</xsl:text>
                <xsl:value-of select="tei:orig"/>
            </xsl:attribute>
            <xsl:value-of select="tei:reg"/>
        </correction>
    </xsl:template>
    
</xsl:stylesheet>
