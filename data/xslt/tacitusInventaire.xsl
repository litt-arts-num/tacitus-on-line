<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tei" version="2.0">
    <xsl:output method="html" omit-xml-declaration="yes" indent="yes"/>

    <xsl:template match="/tei:TEI">
        <p><span class="lead">Les facsimilés de l'édition de 1608 (Paris, Pierre Chevallier)</span> sont gracieusement mis à
            disposition par la Bibliothèque Municipale de Lyon qui a pris en charge la numérisation
            de l'ouvrage disponible sous la cote BmL 23955. Elles sont hébergées sur l'entrepôt de
            données Nakala opéré par l'IR* Huma-Num.</p>
        <xsl:apply-templates select="//tei:table[@xml:id = 'inventory']"/>
    </xsl:template>

    <xsl:template match="tei:table">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"/>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text" id="basic-addon1">
                    <i class="fa fa-filter"/>
                </span>
            </div>
            <input class="form-control" id="myInput" type="text" placeholder="Filtrer..."/>
        </div>
        <div class="mb-3">
            <p>Astuce : pour rechercher une page spécifique du pdf, par exemple, la page 10,
                chercher "<code>§10 </code>" (notez l'espace finale)</p>
        </div>

        <table id="inventaire"
            class="table table-light table-striped table-sm table-bordered table-hover">
            <thead>
                <tr>
                    <xsl:for-each select="tei:row[@role = 'label']/tei:cell">
                        <th>
                            <xsl:value-of select="."/>
                        </th>
                    </xsl:for-each>
                </tr>
            </thead>
            <tbody id="myTable">
                <xsl:for-each select="tei:row[not(@role = 'label')]">
                    <tr>
                        <td>
                            <xsl:value-of select="tei:cell[1]"/>
                        </td>
                        <td>
                            <xsl:value-of select="tei:cell[2]"/>
                        </td>
                        <td>
                            <a href="{tei:cell[3]}">
                                <xsl:value-of
                                    select="substring-before(substring-after(tei:cell[3], 'iiif/'), '/full')"
                                />
                            </a>
                        </td>
                        <td>
                            <xsl:choose>
                                <xsl:when test="not(tei:cell[4] = '')">
                                    <xsl:text>§</xsl:text>
                                    <xsl:value-of select="tei:cell[4]"/>
                                    <xsl:text> </xsl:text>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:text>--</xsl:text>
                                </xsl:otherwise>
                            </xsl:choose>
                        </td>
                    </tr>
                </xsl:for-each>
            </tbody>
        </table>

        <script>
$(document).ready(function(){
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $("#myTable tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>

    </xsl:template>
</xsl:stylesheet>
