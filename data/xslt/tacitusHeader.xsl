<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs tei" version="2.0"
    xmlns:tei="http://www.tei-c.org/ns/1.0">

    <xsl:output method="html" omit-xml-declaration="yes" indent="yes"/>
    <xsl:strip-space elements="*"/>
    <xsl:param name="typeMap">
        <xsl:copy-of select="//tei:list[@xml:id = 'typology_commentary']"/>
    </xsl:param>

    <xsl:template match="tei:TEI">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:teiHeader">
        <xsl:apply-templates select="tei:encodingDesc/tei:projectDesc"/>
        <xsl:apply-templates select="tei:fileDesc/tei:titleStmt"/>
        <xsl:apply-templates select="tei:fileDesc/tei:publicationStmt"/>
        <xsl:apply-templates select="tei:fileDesc/tei:sourceDesc"/>
        <xsl:apply-templates select="tei:encodingDesc"/>
        <xsl:apply-templates select="tei:revisionDesc"/>
    </xsl:template>

    <!-- HOME and ABOUT -->
    <xsl:template match="tei:projectDesc">
        <!-- generate HOME page -->
        <xsl:result-document href="views/html/editorial_home.html" method="xhtml" indent="yes"
            omit-xml-declaration="yes">
            <xsl:call-template name="create_carousel"/>
            <xsl:apply-templates mode="home"/>
        </xsl:result-document>
    </xsl:template>

    <xsl:template match="tei:titleStmt">
        <!-- ABOUT -->
        <!-- generate TEAM subpage -->
        <xsl:result-document href="views/html/editorial_about_team.html" method="xhtml" indent="yes"
            omit-xml-declaration="yes">
            <H3>L'équipe</H3>
            <!-- TODO : Utiliser les dates ? -->
            <ul>
                <li>Responsable du projet : <i><xsl:value-of select="tei:principal"/>.</i></li>
                <xsl:for-each-group select="tei:respStmt" group-by="tei:resp">
                    <xsl:sort order="ascending" select="tei:n"/>
                    <li><xsl:value-of
                            select="concat(upper-case(substring(tei:resp, 1, 1)), substring(tei:resp, 2))"
                        /> : <i><xsl:for-each select="current-group()">
                                <xsl:for-each select="tei:name | tei:orgName | tei:persName">
                                    <xsl:value-of select="."/>
                                    <xsl:choose>
                                        <xsl:when test="position() = (last() - 1)"> et </xsl:when>
                                        <xsl:when test="not(position() = last())">, </xsl:when>
                                        <xsl:when test="position() = last()">.</xsl:when>
                                    </xsl:choose>
                                </xsl:for-each>
                            </xsl:for-each></i>
                    </li>
                </xsl:for-each-group>
            </ul>
        </xsl:result-document>

        <!-- generate FUNDER subpage -->
        <xsl:result-document href="views/html/editorial_about_funder.html" method="xhtml"
            indent="yes" omit-xml-declaration="yes">
            <H3>Soutiens</H3> Le projet a bénéficé du soutien financier de : <ul>
                <xsl:for-each select="tei:funder">
                    <li><xsl:value-of
                            select="concat(upper-case(substring(., 1, 1)), substring(., 2))"/>
                    </li>
                </xsl:for-each>
            </ul> Ainsi que du soutien de <xsl:for-each select="tei:sponsor">
                <xsl:value-of select="."/>
                <xsl:choose>
                    <xsl:when test="position() = (last() - 1)"> et de </xsl:when>
                    <xsl:when test="not(position() = last())">, de </xsl:when>
                    <xsl:when test="position() = last()">.</xsl:when>
                </xsl:choose>
            </xsl:for-each>
        </xsl:result-document>
    </xsl:template>

    <xsl:template match="tei:revisionDesc">
        <!-- generate HISTORY subpage -->
        <!-- TODO: Process multiple revisionDesc -->
        <xsl:result-document href="views/html/editorial_about_history.html" method="xhtml"
            indent="yes" omit-xml-declaration="yes">
            <i>De la plus récente à la plus ancienne.</i>
            <ul>
                <xsl:variable name="last_change_num" select="max(tei:change/xs:integer(@n))"/>
                <xsl:if test="tei:change[xs:integer(@n) = $last_change_num]/@status != 'version'">
                    <li>
                        <b>Version en cours...</b>
                    </li>
                </xsl:if>
                <xsl:for-each select="tei:change">
                    <xsl:sort select="@n" order="descending" data-type="number"/>
                    <xsl:choose>
                        <xsl:when test="@status = 'draft' or @status = 'minor'">
                            <ul>
                                <li>
                                    <i class="text-secondary" title="{@n}">
                                        <xsl:apply-templates/>
                                    </i>
                                </li>
                            </ul>
                        </xsl:when>
                        <xsl:when test="@status = 'version'">
                            <li>
                                <b title="{@n}">
                                    <xsl:apply-templates/>
                                </b>
                            </li>
                        </xsl:when>
                    </xsl:choose>
                </xsl:for-each>
            </ul>
        </xsl:result-document>
    </xsl:template>

    <!-- EDITION-->
    <xsl:template match="tei:publicationStmt">
        <!-- generate PUBLICATION subpage -->
        <xsl:result-document href="views/html/editorial_edition_publication.html" method="xhtml"
            indent="yes" omit-xml-declaration="yes">
            <h4>Publication</h4>
            <p> L'édition est publiée par
                <span title="tei:authority"><xsl:value-of select="normalize-space(tei:authority)"
                    /></span>
                 .</p>
            <h4>Diffusion</h4>
            <p>
                <span title="tei:availability">
                    <xsl:apply-templates select="tei:availability"/>
                </span>
            </p>
        </xsl:result-document>
        <!-- generate WEBSITE subpage -->
        <xsl:result-document href="views/html/editorial_about_website.html" method="xhtml"
            indent="yes" omit-xml-declaration="yes">
            <p>
                <xsl:value-of select="normalize-space(tei:pubPlace/tei:note)"/>
            </p>
        </xsl:result-document>
    </xsl:template>

    <xsl:template match="tei:sourceDesc">
        <!-- generate SOURCE subpage -->
        <xsl:result-document href="views/html/editorial_edition_source.html" method="xhtml"
            indent="yes" omit-xml-declaration="yes">
            <h4>Sources</h4>
            <xsl:for-each select="tei:bibl">
                <p>
                    <xsl:value-of select="normalize-space(.)"/>
                </p>
            </xsl:for-each>
        </xsl:result-document>
    </xsl:template>

    <xsl:template match="tei:encodingDesc">
        <!-- generate PRINCIPLES subpage -->
        <xsl:result-document href="views/html/editorial_about_project.html" method="xhtml"
            indent="yes" omit-xml-declaration="yes">
            <!--<H3>Le projet</H3>-->
            <xsl:for-each select="tei:projectDesc/tei:p">
                <xsl:choose>
                    <xsl:when test="exists(./tei:bibl)">
                        <xsl:if test="tei:bibl/@type = 'general'">
                            <div id="project" class="row anchor">
                                <div class="col">
                                    <h3 id="biblio">Bibliographie</h3>
                                    <ul>
                                        <xsl:for-each select="tei:bibl/tei:bibl">
                                            <li>
                                                <xsl:apply-templates select="."/>
                                            </li>
                                        </xsl:for-each>
                                    </ul>
                                </div>
                            </div>
                        </xsl:if>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:apply-templates select="."/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>
        </xsl:result-document>

        <!-- generate PRINCIPLES subpage -->
        <xsl:result-document href="views/html/editorial_edition_principles.html" method="xhtml"
            indent="yes" omit-xml-declaration="yes">
            <h5>Corrections</h5>
            <xsl:for-each select="tei:editorialDecl/tei:correction/tei:p">
                <xsl:apply-templates select="."/>
            </xsl:for-each>

            <h5>Normalisations</h5>
            <xsl:for-each select="tei:editorialDecl/tei:normalization/tei:p">
                <xsl:apply-templates select="."/>
            </xsl:for-each>

            <h5>Interprétations</h5>
            <xsl:for-each select="tei:editorialDecl/tei:interpretation/tei:p">
                <xsl:apply-templates select="."/>
            </xsl:for-each>

        </xsl:result-document>
    </xsl:template>

    <xsl:template match="tei:text"/>

    <xsl:template match="tei:bibl" mode="home"/>

    <xsl:template match="tei:p" mode="#all">
        <p class="text-justify">
            <xsl:apply-templates mode="home"/>
            <!--<xsl:if test="not(substring(., string-length(.)) = '.')">
                <xsl:text>.</xsl:text>
            </xsl:if>-->
        </p>
    </xsl:template>

    <xsl:template match="tei:ref" mode="#all">
        <a href="{@target}"><xsl:apply-templates/></a>
    </xsl:template>
    <xsl:template match="tei:figure" mode="home"/>
    <xsl:template match="tei:figDesc" mode="home"/>
    <xsl:template match="tei:note" mode="home"/>

    <xsl:template match="tei:persName" mode="#all">
        <mark title="Nom de personne">
            <xsl:apply-templates mode="#current"/>
        </mark>
    </xsl:template>
    <xsl:template match="tei:list" mode="#all">
        <ul>
            <xsl:for-each select="tei:item">
                <li>
                    <span class="pl-1 pr-1 mr-2">
                        <xsl:attribute name="data-type">
                            <xsl:value-of select="@xml:id"/>
                        </xsl:attribute>
                        <b>
                            <xsl:value-of select="normalize-space(tei:name)"/>
                        </b>
                    </span>
                    <xsl:apply-templates select="* except tei:name"/>
                </li>
            </xsl:for-each>
        </ul>
    </xsl:template>

    <xsl:template match="tei:title[@level = 'm']" mode="#all">
        <i>
            <xsl:value-of select="."/>
        </i>
    </xsl:template>

    <xsl:template name="create_carousel">
        <div id="carouselExampleIndicators" class="carousel slide float-right ml-3"
            data-ride="carousel">
            <!--<ol class="carousel-indicators">
                <xsl:for-each select=".//tei:figure">
                    <xsl:sort select="@n" order="ascending"/>
                    <li data-target="#carouselExampleIndicators">
                        <xsl:attribute name="data-slide-to">
                            <xsl:value-of select="position()"/>
                        </xsl:attribute>
                        <xsl:if test="position() = 1">
                            <xsl:attribute name="class">active</xsl:attribute>
                        </xsl:if>
                    </li>
                </xsl:for-each>
            </ol>-->
            <div class="carousel-inner">
                <xsl:attribute name="style">
                    <xsl:text>width: 250px;</xsl:text>
                </xsl:attribute>
                <xsl:for-each select=".//tei:figure">
                    <xsl:sort select="@n" order="ascending"/>
                    <div>
                        <xsl:attribute name="class">
                            <xsl:text>carousel-item</xsl:text>
                            <xsl:if test="position() = 1"> active</xsl:if>
                        </xsl:attribute>
                        <figure class="figure">
                            <img class="d-block rounded">
                                <xsl:attribute name="src">
                                    <xsl:value-of select="@facs"/>
                                </xsl:attribute>
                                <xsl:attribute name="style">
                                    <xsl:text>width: 250px; max-height: 400px;</xsl:text>
                                </xsl:attribute>
                                <xsl:attribute name="title">
                                    <xsl:value-of select="tei:figDesc"/>
                                    <xsl:if test="tei:note[@type = 'rights']">
                                        <xsl:text> (crédits : </xsl:text>
                                        <xsl:value-of select="tei:note"/>
                                        <xsl:text>)</xsl:text>
                                    </xsl:if>
                                </xsl:attribute>
                            </img>
                            <figcaption class="figure-caption">
                                <xsl:value-of select="tei:figDesc"/>
                                <xsl:if test="tei:note[@type = 'rights']">
                                    <xsl:text> (crédits : </xsl:text>
                                    <xsl:value-of select="tei:note"/>
                                    <xsl:text>)</xsl:text>
                                </xsl:if>
                            </figcaption>
                        </figure>
                    </div>
                </xsl:for-each>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"/>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button"
                data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"/>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </xsl:template>
</xsl:stylesheet>
